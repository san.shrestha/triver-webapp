import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import CircularProgress from '@material-ui/core/CircularProgress';
import EditRounded from '@material-ui/icons/EditRounded';
import SaveRounded from '@material-ui/icons/SaveRounded';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const styles = theme => ({
    formControl: {
        minWidth: 225,
    },
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: 30,
        textAlign: 'center',
        color: 'red',
    },
    pageTitle: {
        paddingTop: 10,
        paddingBottom: 20
    },
    textField: {

    },
    button: {
        marginLeft:30
    },
    floatRight: {
      float: 'right',
      marginRight: 5,
      marginLeft: 5
    },
    textField: {
      minWidth: 250
    },
    dateField: {
      marginTop:15,
      minWidth: 250
    },
    select: {
      marginTop:15,
      minWidth: 250
    },
    fullWidth: {
      width: '100%'
    }
});

class Profile extends Component {
    constructor(props){
      super(props);

      this.state = {
        initialState : null,
        clientId: "",
        phone: "",
        street: "",
        city: "",
        state: "",
        zipcode: "",
        password_recovery_q1: "",
        password_recovery_a1: "",
        isPasswordRecoveryUsingText: true,
        languagePreference: "",
        creditcard: "",
        nameOnCard: "",
        credit_expiration_date: "",
        message: "",
        isError: "",
        firstName: "",
        lastName: "",
        userName: "",
        email: "",
        password: "",
        languageKnown: [],
        secondaryPhone: "",
        emergencyContactPerson: "",
        emergencyContactPhone: "",
        riderImageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQMW5XEbxz082f1Q46B88ywrOs_hM9dpF7LFL9q98C5NgYUFTdQA',

        mode: 'read',
        loginMode: "read",
        showLoginMode: true,
        riderInfoMode: "read",
        showRiderInfoMode: true,
        accountInfoMode: "read",
        showAccountInfoMode: true,

        activeStep: 0,
        isLoading: false,
        showEULA: false,
        showAnswer: false,
        logged : false,

        riderInfoURL: "/api/rider",
        updateRiderURL : "/api/updaterider",
        riderPaymentURL: "/api/driverPayment",
      }

      this.setProfileDetails = this.setProfileDetails.bind(this);
      this.editModeToggle = this.editModeToggle.bind(this);
      this.setInitialState = this.setInitialState.bind(this);

      this.updatedProfileDetails = this.updatedProfileDetails.bind(this);
      this.toggleModes = this.toggleModes.bind(this);
      this.saveAccountInfo = this.saveAccountInfo.bind(this);
      this.handleEULAAccept = this.handleEULAAccept.bind(this);
    }

    saveAccountInfo() {
      var userinfo = this.props.getLoggedUserInfo();
      if(userinfo.userid && userinfo.token){
        var userid = userinfo.userid;
        var token = userinfo.token;
        let url = this.state.riderPaymentURL;
        let body = this.getCurrentUserInfo(userid);

        let options = {
          method: 'POST',
          headers: {
            "Content-Type": "application/json",
            "cache-control": "no-cache",
            "Authorization": "Bearer " + token
          },
          body: JSON.stringify(body)
        }
        this.ajaxCall(url, options, this.setProfileDetails)
      }
    }

    updatedProfileDetails(){
      this.toggleModes(null);
    }

    toggleModes(activeMode) {
      let modes = ["showLoginMode", "showRiderInfoMode", "showAccountInfoMode"];

      for(var i = 0; i < modes.length; i++){
        let mode = modes[i];
        let modeValue = !activeMode || (mode == activeMode);
        this.setState({ [mode]: modeValue });
      }

      if(!activeMode){
        let displayModes = ["loginMode", "riderInfoMode", "accountInfoMode"];
        for(var i = 0; i < displayModes.length; i++){
          let mode = displayModes[i];
          this.setState({ [mode]: "read" });
        }
      }
    }

    editLoginModeToggle() {
      this.setState(prevState => (
      {
        loginMode: this.state.loginMode == 'read' ? 'edit' : 'read'
      }));
      if(this.state.loginMode == 'read'){
        this.toggleModes("showLoginMode");
      }
      else{
        this.toggleModes(null);
      }
    }

    editRiderInfoModeToggle() {
      this.setState(prevState => (
      {
        riderInfoMode: this.state.riderInfoMode == 'read' ? 'edit' : 'read'
      }));
      if(this.state.riderInfoMode == 'read'){
        this.toggleModes("showRiderInfoMode");
      }
      else{
        this.toggleModes(null);
      }
    }

    editAccountInfoModeToggle(){
      this.setState(prevState => (
      {
        accountInfoMode: this.state.accountInfoMode == 'read' ? 'edit' : 'read'
      }));
      if(this.state.accountInfoMode == 'read'){
        this.toggleModes("showAccountInfoMode");
      }
      else{
        this.toggleModes(null);
      }
    }

    handleEULAAccept() {
      let self = this;
      self.toggleEULA(false);
      self.setState(prevState => (
      {
        isEULAAccepted: 1,
      }));

      setTimeout(function(){
          self.updateLoggedProfileDetails();
      }, 100)
    }

    toggleEULA(show) {
      this.setState(prevState => (
      {
        showEULA: show,
      }));
    }

    toggleLoading(loading) {
      this.setState(prevState => (
      {
        isLoading: loading,
      }));
    }

    editModeToggle() {
      this.setState(prevState => (
      {
        mode: this.state.mode == 'read' ? 'edit' : 'read'
      }));
    }

    componentDidMount() {
      var userinfo = this.props.getLoggedUserInfo();
      if(userinfo.userid && userinfo.token){
        this.getLoggedProfileDetails(userinfo);
      }
      else{
        this.setState(prevState => (
        {
          mode: 'edit'
        }));
      }
    }

    setProfileDetails(data){
      data = JSON.parse(data);
      console.log("data:");
      console.log(data);

      this.setState(prevState => (
      {
        initialState : data,
        clientId: data.clientId,
        phone: data.phone,
        street: data.street,
        city: data.city,
        state: !(data.state) ? '' : data.state,
        zipcode: data.zipcode,
        password_recovery_q1: data.password_recovery_q1,
        password_recovery_a1: data.password_recovery_a1,
        isPasswordRecoveryUsingText: data.isPasswordRecoveryUsingText,
        languagePreference: data.languagePreference,
        creditcard: data.creditcard,
        nameOnCard: (data.firstName + ' ' + data.lastName),
        credit_expiration_date: data.credit_expiration_date,
        message: data.message,
        isError: data.isError,
        firstName: data.firstName,
        lastName: data.lastName,
        userName: data.userName,
        email: data.email,
        languageKnown: data.languageKnown == null ? [] :  data.languageKnown,
        secondaryPhone: data.secondaryPhone,
        emergencyContactPerson: data.emergencyContactPerson,
        emergencyContactPhone: data.emergencyContactPhone,
        //riderImageUrl:data.riderImageUrl,

        logged: true
      }));

      console.log(this.state);
    }

    setInitialState() {
      this.setState(prevState => (
      {
        mode: 'read',
      }));

      var data = this.state.initialState;
      if(data == null)
        return;

      this.setState(prevState => (
      {
        clientId: data.clientId,
        phone: data.phone,
        street: data.street,
        city: data.city,
        state: !(data.state) ? '' : data.state,
        zipcode: data.zipcode,
        password_recovery_q1: data.password_recovery_q1,
        password_recovery_a1: data.password_recovery_a1,
        isPasswordRecoveryUsingText: data.isPasswordRecoveryUsingText,
        languagePreference: data.languagePreference,
        creditcard: data.creditcard,
        nameOnCard: (data.firstName + ' ' + data.lastName),
        credit_expiration_date: data.credit_expiration_date,
        message: data.message,
        isError: data.isError,
        firstName: data.firstName,
        lastName: data.lastName,
        userName: data.userName,
        email: data.email,
        languageKnown: data.languageKnown == null ? [] :  data.languageKnown,
        secondaryPhone: data.secondaryPhone,
        emergencyContactPerson: data.emergencyContactPerson,
        emergencyContactPhone: data.emergencyContactPhone,
        //riderImageUrl:data.riderImageUrl,

        logged: true
      }));
    }

    getLoggedProfileDetails(userinfo){
      var userid = userinfo.userid;
      var token = userinfo.token;
      let url = this.state.riderInfoURL;
      let body = {
        "userId": userid
      }
      let options = {
        method: 'POST',
        headers: {
          "Content-Type": "application/json",
          "cache-control": "no-cache",
          "Authorization": "Bearer " + token
        },
        body: JSON.stringify(body)
      }
      this.ajaxCall(url, options, this.setProfileDetails)
    }

    updateLoggedProfileDetails(userinfo){
      var userinfo = this.props.getLoggedUserInfo();
      if(userinfo.userid && userinfo.token){
        var userid = userinfo.userid;
        var token = userinfo.token;
      }

      let headers = {
        "Content-Type": "application/json",
        "cache-control": "no-cache"
      };
      if(userinfo.userid && userinfo.token){
        var token = userinfo.token;
        headers.Authorization = "Bearer " + token
      }

      let url = this.state.updateRiderURL;;
      let body = this.state;
      let options = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(body)
      }

      //this.ajaxCall(url, options, this.updatedProfileDetails);
      this.ajaxCall(url, options, null);
    }

    ajaxCall = (url, options, callback) => {
      let self = this;
      self.toggleLoading(true);
      fetch(url, options)
          .then(function (response) {
          return response.json();
      })
      .then(function (result) {
          if(!result.error){
            callback(result);
          }
          else {
            alert('error signing in');
          }
          self.toggleLoading(false);
      })
      .catch (function (error) {
        self.toggleLoading(false);
        console.log(error);
      });
    }

    handleNext = () => {
      if(this.state.activeStep === this.getSteps().length - 1){
        this.props.handleProfileComplete();
      }
      else{
       this.setState(state => ({
         activeStep: state.activeStep + 1,
       }));
     }
   };

   handleBack = () => {
     this.setState(state => ({
       activeStep: state.activeStep - 1,
     }));
   };

    handleInputChange = prop => event => {
        this.setState({ [prop]: event.target.value });
    };

    getStepContent(stepIndex) {
      const { classes } = this.props;
      switch (stepIndex) {
        case 0:
          return(
            <div>
              {this.state.loginMode != 'read'
                ?<div>
                  <SaveRounded className={classes.floatRight}
                      color="primary"
                      onClick={() => this.editLoginModeToggle()}>
                    <Visibility />
                  </SaveRounded>
                </div>
                : null
              }
              {this.state.loginMode == 'read' && this.state.showLoginMode
                ?<EditRounded className={classes.floatRight}
                    color="primary"
                    onClick={() => this.editLoginModeToggle()}>
                    <Visibility />
                  </EditRounded>
                :
                  null
              }
            <h3> Login Information</h3>
            <br/>
            <Grid container spacing={24}>

                <Grid item xs={12} sm={12}>
                  {this.state.loginMode == 'read'
                    ?
                      <div>
                        <InputLabel>Username</InputLabel>
                        <br/>
                        <h6>{this.state.userName}&nbsp;</h6>
                      </div>
                    :
                      <TextField
                          id="standard-with-placeholder"
                          label="User Name"
                          placeholder="Please enter a valid email address"
                          className={classes.textField}
                          margin="normal"
                          fullWidth
                          value={this.state.userName}
                          onChange={this.handleInputChange('userName')}/>
                    }
                </Grid>
                <Grid item xs={12} sm={12}>
                  {this.state.loginMode == 'read'
                    ?
                      <div>
                        <InputLabel>Email</InputLabel>
                        <br/>
                        <h6>{this.state.email}&nbsp;</h6>
                      </div>
                    :
                      <TextField
                          id="standard-with-placeholder"
                          label="Email"
                          placeholder="Please enter a valid email address"
                          className={classes.textField}
                          margin="normal"
                          fullWidth
                          value={this.state.email}
                          onChange={this.handleInputChange('email')}/>
                    }
                </Grid>
                {this.state.loginMode == 'read'
                  ?
                    null
                  :
                    <Grid item xs={12} sm={12}>
                      <TextField
                          id="standard-with-placeholder"
                          label="Password"
                          placeholder="Password"
                          className={classes.textField}
                          type="password"
                          margin="normal"
                          fullWidth
                      />
                    </Grid>
                }
                {this.state.loginMode == 'read'
                  ?
                    null
                  :
                    <Grid item xs={12} sm={12}>
                      <TextField
                        id="standard-with-placeholder"
                        label="Password Confirmation"
                        placeholder="Password Confirmation"
                        className={classes.textField}
                        type="password"
                        margin="normal"
                        fullWidth
                    />
                  </Grid>
                }
                <Grid item xs={12} sm={12}>
                  {this.state.loginMode == 'read'
                    ?
                      <div>
                        <InputLabel>Recovery Question</InputLabel>
                        <br/>
                        <h6>{this.state.password_recovery_q1}&nbsp;</h6>
                      </div>
                    :
                    <FormControl className={classes.formControl, classes.fullWidth}>
                      <InputLabel htmlFor="password_recovery_q1">Recovery Question</InputLabel>
                      <Select
                        value={this.state.password_recovery_q1}
                        onChange={this.handleInputChange('password_recovery_q1')}
                        inputProps={{
                          name: 'password_recovery_q1',
                          id: 'password_recovery_q1',
                        }}>
                        <MenuItem value="What is the first concert you attended?">What is the first concert you attended?</MenuItem>
                        <MenuItem value="What is the your mother's maiden name?">What is the your mother's maiden name?</MenuItem>
                        <MenuItem value="In what city were you born?">In what city were you born?</MenuItem>
                        <MenuItem value="What is your favorite hobby">In what city were you born?</MenuItem>
                      </Select>
                    </FormControl>
                    }
                </Grid>
                <Grid item xs={12} sm={12}>
                  {this.state.loginMode == 'read'
                    ?
                      null
                    :
                      <TextField
                          label="Recovery Answer"
                          placeholder="Recovery Answer"
                          className={classes.textField}
                          margin="normal"
                          type={this.state.showAnswer ? 'text' : 'password'}
                          fullWidth
                          value={this.state.password_recovery_a1}
                          onChange={this.handleInputChange('password_recovery_a1')}
                          InputProps={{
                              endAdornment: (
                                  <InputAdornment position="end">
                                      <IconButton
                                          tabIndex={-1}
                                          aria-label="Toggle password visibility"
                                          onClick={this.handleShowAnswer}>
                                          {this.state.showAnswer
                                              ? <VisibilityOff />
                                              : <Visibility />}
                                      </IconButton>
                                  </InputAdornment>
                              ),
                          }}
                      />
                    }
                </Grid>
            </Grid>
            </div>
          );
        case 1:
          return(
            <div>

            {this.state.riderInfoMode != 'read'
              ?<div>
                <SaveRounded className={classes.floatRight}
                    color="primary"
                    onClick={() => this.updateLoggedProfileDetails()}>
                  <Visibility />
                </SaveRounded>
              </div>
              : null
            }
            {
              this.state.riderInfoMode == 'read' && this.state.showRiderInfoMode
              ?
                <EditRounded className={classes.floatRight}
                  color="primary"
                  onClick={() => this.editRiderInfoModeToggle()}>
                  <Visibility />
                </EditRounded>
              :
                null
            }

            <h3>Profile Information</h3>
            <br/>
            <Grid container spacing={24}>
                <Grid item xs={12} sm={this.state.riderInfoMode == 'read' ? 12 : 6}>
                {this.state.riderInfoMode == 'read'
                  ?
                   <div>
                     <InputLabel>Name</InputLabel>
                     <br/>
                     <h6>{this.state.firstName + '  ' + this.state.lastName}&nbsp;</h6>
                   </div>
                  :
                    <TextField
                        id="standard-with-placeholder"
                        label="First Name"
                        placeholder="First Name"
                        className={classes.textField}
                        margin="normal"
                        value={this.state.firstName}
                        onChange={this.handleInputChange('firstName')}
                    />
                  }
                </Grid>
                {this.state.riderInfoMode == 'read'
                  ? null
                  :
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="standard-with-placeholder"
                        label="Last Name"
                        placeholder="Last Name"
                        className={classes.textField}
                        margin="normal"
                        value={this.state.lastName}
                        onChange={this.handleInputChange('lastName')}
                    />
                </Grid>
                }

                <Grid item xs={12} sm={12}>
                  {this.state.riderInfoMode == 'read'
                    ?
                      <div>
                        <InputLabel>Street</InputLabel>
                        <br/>
                        <h6>{this.state.street}&nbsp;</h6>
                      </div>
                    :

                        <TextField
                            id="standard-with-placeholder"
                            label="Street"
                            placeholder="Street"
                            className={classes.textField}
                            margin="normal"
                            fullWidth
                            value={this.state.street}
                            onChange={this.handleInputChange('street')}
                        />
                }
              </Grid>

                <Grid item xs={12} sm={6}>
                {this.state.riderInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>City</InputLabel>
                      <br/>
                      <h6>{this.state.city}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        id="standard-with-placeholder"
                        label="City"
                        placeholder="City"
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.city}
                        onChange={this.handleInputChange('city')}
                    />
                  }
                </Grid>
                <Grid item xs={12} sm={6}>
                {this.state.riderInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>State</InputLabel>
                      <br/>
                      <h6>{this.state.State}&nbsp;</h6>
                    </div>
                  :
                    <FormControl className={classes.formControl, classes.select}>
                      <InputLabel htmlFor="riderState">state</InputLabel>
                      <Select
                        value={this.state.state}
                        onChange={this.handleInputChange('state')}
                        inputProps={{
                          name: 'state',
                          id: 'riderState',
                        }}>
                        <MenuItem value="">Select</MenuItem>
                        <MenuItem value="AK">AK</MenuItem>
                        <MenuItem value="AL">AL</MenuItem>
                        <MenuItem value="AR">AR</MenuItem>
                        <MenuItem value="AZ">AZ</MenuItem>
                        <MenuItem value="CA">CA</MenuItem>
                        <MenuItem value="CO">CO</MenuItem>
                        <MenuItem value="CT">CT</MenuItem>
                        <MenuItem value="DC">DC</MenuItem>
                        <MenuItem value="DE">DE</MenuItem>
                        <MenuItem value="FL">FL</MenuItem>
                        <MenuItem value="GA">GA</MenuItem>
                        <MenuItem value="HI">HI</MenuItem>
                        <MenuItem value="IA">IA</MenuItem>
                        <MenuItem value="ID">ID</MenuItem>
                        <MenuItem value="IL">IL</MenuItem>
                        <MenuItem value="IN">IN</MenuItem>
                        <MenuItem value="KS">KS</MenuItem>
                        <MenuItem value="KY">KY</MenuItem>
                        <MenuItem value="LA">LA</MenuItem>
                        <MenuItem value="MA">MA</MenuItem>
                        <MenuItem value="MD">MD</MenuItem>
                        <MenuItem value="ME">ME</MenuItem>
                        <MenuItem value="MI">MI</MenuItem>
                        <MenuItem value="MN">MN</MenuItem>
                        <MenuItem value="MO">MO</MenuItem>
                        <MenuItem value="MS">MS</MenuItem>
                        <MenuItem value="MT">MT</MenuItem>
                        <MenuItem value="NC">NC</MenuItem>
                        <MenuItem value="ND">ND</MenuItem>
                        <MenuItem value="NE">NE</MenuItem>
                        <MenuItem value="NH">NH</MenuItem>
                        <MenuItem value="NJ">NJ</MenuItem>
                        <MenuItem value="NM">NM</MenuItem>
                        <MenuItem value="NV">NV</MenuItem>
                        <MenuItem value="NY">NY</MenuItem>
                        <MenuItem value="OH">OH</MenuItem>
                        <MenuItem value="OK">OK</MenuItem>
                        <MenuItem value="OR">OR</MenuItem>
                        <MenuItem value="PA">PA</MenuItem>
                        <MenuItem value="RI">RI</MenuItem>
                        <MenuItem value="SC">SC</MenuItem>
                        <MenuItem value="SD">SD</MenuItem>
                        <MenuItem value="TN">TN</MenuItem>
                        <MenuItem value="TX">TX</MenuItem>
                        <MenuItem value="UT">UT</MenuItem>
                        <MenuItem value="VA">VA</MenuItem>
                        <MenuItem value="VT">VT</MenuItem>
                        <MenuItem value="WA">WA</MenuItem>
                        <MenuItem value="WI">WI</MenuItem>
                        <MenuItem value="WV">WV</MenuItem>
                        <MenuItem value="WY">WY</MenuItem>
                      </Select>
                    </FormControl>
                  }
                </Grid>
                <Grid item xs={12} sm={6}>
                {this.state.riderInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Primary Phone Number</InputLabel>
                      <br/>
                      <h6>{this.state.phone}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        id="standard-with-placeholder"
                        label="Primary Phone Number"
                        placeholder="Primary Phone Number"
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.phone}
                        onChange={this.handleInputChange('phone')}
                    />
                  }
                </Grid>
                <Grid item xs={12} sm={6}>
                {this.state.riderInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Secondary Phone Number</InputLabel>
                      <br/>
                      <h6>{this.state.secondaryPhone}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        id="standard-with-placeholder"
                        label="Secondary Phone Number"
                        placeholder="Secondary Phone Number"
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.secondaryPhone}
                        onChange={this.handleInputChange('secondaryPhone')}
                    />
                  }
                </Grid>
                <Grid item xs={12} sm={6}>
                {this.state.riderInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Emergency Contact Person</InputLabel>
                      <br/>
                      <h6>{this.state.emergencyContactPerson}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        id="standard-with-placeholder"
                        label="Emergency Contact Person"
                        placeholder="Emergency Contact Person"
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.emergencyContactPerson}
                        onChange={this.handleInputChange('emergencyContactPerson')}
                    />
                  }
                </Grid>

                <Grid item xs={12} sm={6}>
                {this.state.riderInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Emergency Contact Phone</InputLabel>
                      <br/>
                      <h6>{this.state.emergencyContactPhone}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        id="standard-with-placeholder"
                        label="Emergency Contact Phone"
                        placeholder="Emergency Contact Person"
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.emergencyContactPhone}
                        onChange={this.handleInputChange('emergencyContactPhone')}
                    />
                  }
                </Grid>

                <Grid item xs={12} sm={6}>
                {this.state.riderInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Languages</InputLabel>
                      <br/>
                      <h6>{this.state.languagePreference}&nbsp;</h6>
                    </div>
                  :
                    <FormControl className={classes.formControl}>
                      <InputLabel htmlFor="languageKnown">Languages</InputLabel>
                      <Select
                        multiple
                        value={this.state.languageKnown}
                        onChange={this.handleInputChange('languageKnown')}
                        inputProps={{
                          name: 'languageKnown',
                          id: 'languageKnown',
                        }}>
                        <MenuItem value="Chinese">Chinese</MenuItem>
                        <MenuItem value="English">English</MenuItem>
                        <MenuItem value="Hindi">Hindi</MenuItem>
                        <MenuItem value="Nepali">Nepali</MenuItem>
                        <MenuItem value="Spanish">Spanish</MenuItem>
                      </Select>
                    </FormControl>
                  }
                </Grid>
              </Grid>
            </div>
          );
        case 2:
          return(
            <div>
            {this.state.accountInfoMode != 'read'
              ?<div>
                <SaveRounded className={classes.floatRight}
                    color="primary"
                    onClick={() => this.saveAccountInfo()}>
                  <Visibility />
                </SaveRounded>
              </div>
              : null
            }
            {this.state.accountInfoMode == 'read' && this.state.showAccountInfoMode
              ?
                <EditRounded className={classes.floatRight}
                  color="primary"
                  onClick={() => this.editAccountInfoModeToggle()}>
                  <Visibility />
                </EditRounded>
              :
                null
            }

            <h3> Payment Information</h3>
            <br/>
            <Grid container spacing={24}>
                <Grid item xs={12} sm={12}>
                  {this.state.accountInfoMode == 'read'
                    ?
                      <div>
                        <InputLabel>Name on card</InputLabel>
                        <br/>
                        <h6>{this.state.nameOnCard}&nbsp;</h6>
                      </div>
                    :
                      <TextField
                        id="standard-with-placeholder"
                        label="Name on card"
                        placeholder="credit card no."
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.nameOnCard}
                        onChange={this.handleInputChange('nameOnCard')}
                    />
                  }
                </Grid>

                <Grid item xs={12} sm={12}>
                  {this.state.accountInfoMode == 'read'
                    ?
                      <div>
                        <InputLabel>Credit card</InputLabel>
                        <br/>
                        <h6>{this.state.creditcard}&nbsp;</h6>
                      </div>
                    :
                      <TextField
                        id="standard-with-placeholder"
                        label="credit card"
                        placeholder="credit card no."
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.creditcard}
                        onChange={this.handleInputChange('creditcard')}
                    />
                  }
                </Grid>

                <Grid item xs={12} sm={12}>
                  {this.state.accountInfoMode == 'read'
                    ?
                      <div>
                        <InputLabel>Expiration Date</InputLabel>
                        <br/>
                        <h6>{this.state.credit_expiration_date}&nbsp;</h6>
                      </div>
                    :
                      <TextField
                          id="standard-with-placeholder"
                          label="Expiration Date"
                          placeholder="expiration date"
                          className={classes.textField}
                          margin="normal"
                          fullWidth
                          value={this.state.credit_expiration_date}
                          onChange={this.handleInputChange('credit_expiration_date')}
                      />
                    }
                </Grid>
              </Grid>
            </div>
          );
        default:
          return "No steps";
      }
    }

    getSteps() {
      return ['Login Information', 'Profile Information', 'Payment Information'];
    }

    render() {
      const { classes } = this.props;
      const steps = this.getSteps();
      const { activeStep } = this.state;

      return (
        <div className={classes.background}>
          <div className="container">
              <div className="container">
                <h2 className={classes.pageTitle}>Profile Information</h2>
                  <Paper style={{ padding: 20 }}>
                    <Paper style={{ margin: 30, padding: 30 }}>
                      <div className={classes.root}>
                        <Stepper activeStep={activeStep} alternativeLabel>
                          {steps.map(label => {
                            return (
                              <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                              </Step>
                            );
                          })}
                        </Stepper>
                        <div>
                          <br/>
                            <div>
                              <div>
                                {this.getStepContent(activeStep)}
                              </div>
                              <div>
                                <br/>
                                <Button
                                  disabled={activeStep === 0 || !this.state.showLoginMode
                                                             || !this.state.showRiderInfoMode
                                                             || !this.state.showAccountInfoMode}
                                  onClick={this.handleBack}
                                  className={classes.backButton}
                                >
                                  Back
                                </Button>
                                <Button variant="contained" color="primary" onClick={this.handleNext}
                                  disabled={!this.state.showLoginMode
                                            || !this.state.showRiderInfoMode
                                            || !this.state.showAccountInfoMode}
                                >
                                  {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                </Button>
                              </div>
                            </div>
                        </div>
                      </div>
                    </Paper>
                </Paper>
            </div>
          </div>

          <Dialog
            open={this.state.showEULA}
            aria-labelledby="responsive-dialog-title">
            <DialogTitle id="responsive-dialog-title">{"End User License Agreement"}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                What is Lorem Ipsum?
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

                Why do we use it?
                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


                Where does it come from?
                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

                The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.

                Where can I get some?
                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleEULAAccept} color="primary" autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>

          <Dialog
            open={this.state.isLoading}
            aria-labelledby="responsive-dialog-title">
            <DialogContent>
              <CircularProgress />
            </DialogContent>
          </Dialog>
        </div>
      );
  }
};

// export default profile
export default withStyles(styles)(Profile);
