import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Icon from '@material-ui/core/Icon';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import EditRounded from '@material-ui/icons/EditRounded';
import SaveRounded from '@material-ui/icons/SaveRounded';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import { withRouter } from 'react-router-dom';


const styles = theme => ({
    formControl: {
        minWidth: 225,
    },
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: 30,
        textAlign: 'center',
        color: 'red',
    },
    pageTitle: {
        paddingTop: 10,
        paddingBottom: 20
    },
    button: {
        marginLeft:30
    },
    floatRight: {
      float: 'right',
      marginRight: 5,
      marginLeft: 5
    },
    textField: {
      minWidth: 250
    },
    dateField: {
      marginTop:15,
      minWidth: 250
    },
    select: {
      marginTop:15,
      minWidth: 250
    },
    fullWidth: {
      width: '100%'
    }
});

class Profile extends Component  {
  constructor(props){
    super(props);

    var userinfo = this.props.getLoggedUserInfo();
    if(userinfo.userid && userinfo.token){
      this.getLoggedProfileDetails(userinfo);
    }
    else{
      this.props.history.push('/Signup');
    }

    this.state = {
        initialState: null,
        accountNumber: "",
        bankName: "",
        carBackImageUrl: "",
        carBodyType: "",
        carColor: "",
        carFrontImageUrl: "",
        carFuelType: "",
        carMake: "",
        carModel: "",
        carNo: "",
        carRegistrationState: "",
        carVinNumber: "",
        carYear: "",
        city: "",
        credit_expiration_date: "",
        creditcard: "",
        dateOfBirth: "",
        deviceId: "N/A",
        dl_expiration_date: "",
        dl_issue_date: "",
        dl_number: "",
        driverId: "",
        driverImageUrl: "",
        email: "",
        firstName: "",
        emergencyContactPerson: "",
        emergencyContactPhone: "",
        isEULAAccepted: "",
        isPasswordRecoveryUsingText: 0,
        languageKnown: [],
        languagePreference: "",
        lastName: "",
        password_recovery_a1: "",
        password_recovery_q1: "",
        phone: "",
        routingNumber: "",
        secondaryPhone: "",
        ssnNumber: "",
        state: "",
        street: "",
        triverName: "",
        userId: "",
        username: "",
        zipcode: "",

        loginMode: "read",
        showLoginMode: true,
        driverInfoMode: "read",
        showDriverInfoMode: true,
        vehicleInfoMode: "read",
        showVehicleInfoMode: true,
        accountInfoMode: "read",
        showAccountInfoMode: true,

        activeStep: 0,
        isLoading: false,
        showEULA: false,
        showAnswer: false,
        logged : false,
        driverInfoURL : "/api/driver",
        updateLoggedProfileURL: "/api/updatedriver",
        driverPaymentURL: "/api/driverPayment",
    };

    this.setProfileDetails = this.setProfileDetails.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.updatedProfileDetails = this.updatedProfileDetails.bind(this);
    this.toggleModes = this.toggleModes.bind(this);
    this.saveAccountInfo = this.saveAccountInfo.bind(this);
    this.handleEULAAccept = this.handleEULAAccept.bind(this);
  }

  getSteps() {
    return ['Login Information', 'Driver Information', 'Vehicle Information', 'Bank Information'];
  }

  getStepContent(stepIndex) {
    const { classes } = this.props;
    switch (stepIndex) {
      case 0:
        return (
        <div>
          {this.state.loginMode != 'read'
            ?<div>
              <SaveRounded className={classes.floatRight}
                  color="primary"
                  onClick={() => this.editLoginModeToggle()}>
                <Visibility />
              </SaveRounded>
            </div>
            : null
          }
          {this.state.loginMode == 'read' && this.state.showLoginMode
            ?<EditRounded className={classes.floatRight}
                color="primary"
                onClick={() => this.editLoginModeToggle()}>
                <Visibility />
              </EditRounded>
            :
              null
          }
          <h3> Login Information</h3>
          <br/>
          <Grid container spacing={24}>

            <Grid item xs={12} sm={12}>
              {this.state.loginMode == 'read'
                ?
                  <div>
                    <InputLabel>Username</InputLabel>
                    <br/>
                    <h6>{this.state.userName}&nbsp;</h6>
                  </div>
                :
                  <TextField
                      label="User Name"
                      placeholder="Please enter a valid email address"
                      className={classes.textField}
                      margin="normal"
                      fullWidth
                      value={this.state.userName}
                      onChange={this.handleInputChange('userName')}/>
                }
            </Grid>
            <Grid item xs={12} sm={12}>
              {this.state.loginMode == 'read'
                ?
                  <div>
                    <InputLabel>Email</InputLabel>
                    <br/>
                    <h6>{this.state.email}&nbsp;</h6>
                  </div>
                :
                  <TextField
                      label="Email"
                      placeholder="Please enter a valid email address"
                      className={classes.textField}
                      margin="normal"
                      fullWidth
                      value={this.state.email}
                      onChange={this.handleInputChange('email')}/>
                }
            </Grid>
            {
              this.state.loginMode == 'read'
                ? null
                :
              <Grid item xs={12} sm={12}>
                  <TextField
                      label="Password"
                      placeholder="Password"
                      className={classes.textField}
                      type="password"
                      margin="normal"
                      fullWidth
                  />
              </Grid>
            }
            {this.state.loginMode == 'read'
              ?
                null
              : <Grid item xs={12} sm={12}>
                  <TextField
                      label="Password Confirmation"
                      placeholder="Password Confirmation"
                      className={classes.textField}
                      type="password"
                      margin="normal"
                      fullWidth
                  />
                  </Grid>
            }
            <Grid item xs={12} sm={12}>
              {this.state.loginMode == 'read'
                ?
                  <div>
                    <InputLabel>Recovery Question</InputLabel>
                    <br/>
                    <h6>{this.state.password_recovery_q1}&nbsp;</h6>
                  </div>
                :
                <FormControl className={[classes.formControl, classes.fullWidth]}>
                  <InputLabel htmlFor="password_recovery_q1">Recovery Question</InputLabel>
                  <Select
                    value={this.state.password_recovery_q1}
                    onChange={this.handleInputChange('password_recovery_q1')}
                    inputProps={{
                      name: 'password_recovery_q1',
                      id: 'password_recovery_q1',
                    }}>
                    <MenuItem value="What is the first concert you attended?">What is the first concert you attended?</MenuItem>
                    <MenuItem value="What is the your mother's maiden name?">What is the your mother's maiden name?</MenuItem>
                    <MenuItem value="In what city were you born?">In what city were you born?</MenuItem>
                    <MenuItem value="What is your favorite hobby">In what city were you born?</MenuItem>
                  </Select>
                </FormControl>
                
                }
            </Grid>
            <Grid item xs={12} sm={12}>
              {this.state.loginMode == 'read'
                ?
                  null
                :
                  <TextField
                      label="Recovery Answer"
                      placeholder="Recovery Answer"
                      className={classes.textField}
                      margin="normal"
                      type={this.state.showAnswer ? 'text' : 'password'}
                      fullWidth
                      value={this.state.password_recovery_a1}
                      onChange={this.handleInputChange('password_recovery_a1')}
                      InputProps={{
                          endAdornment: (
                              <InputAdornment position="end">
                                  <IconButton
                                      tabIndex={-1}
                                      aria-label="Toggle password visibility"
                                      onClick={this.handleShowAnswer}>
                                      {this.state.showAnswer
                                          ? <VisibilityOff />
                                          : <Visibility />}
                                  </IconButton>
                              </InputAdornment>
                          ),
                      }}
                  />
                }
            </Grid>
            </Grid>
      </div>);

      case 1:
        return (
          <div>
            {this.state.driverInfoMode != 'read'
              ?<div>
                <SaveRounded className={classes.floatRight}
                    color="primary"
                    onClick={() => this.updateLoggedProfileDetails()}>
                  <Visibility />
                </SaveRounded>
              </div>
              : null
            }
            {
              this.state.driverInfoMode == 'read' && this.state.showDriverInfoMode
              ?
                <EditRounded className={classes.floatRight}
                  color="primary"
                  onClick={() => this.editDriverInfoModeToggle()}>
                  <Visibility />
                </EditRounded>
              :
                null
            }
            <h3> Driver Information</h3>
            <Grid container spacing={24}>

                <Grid item xs={12} sm={this.state.driverInfoMode == 'read' ? 12 : 6}>
                  {this.state.driverInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Name</InputLabel>
                      <br/>
                      <h6>{this.state.firstName + '  ' + this.state.lastName}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        label="First Name"
                        placeholder="First Name"
                        className={classes.textField}
                        margin="normal"
                        value={this.state.firstName}
                        onChange={this.handleInputChange('firstName')}
                    />
                  }
                </Grid>

                {this.state.driverInfoMode == 'read'
                  ?
                    null
                  :
                    <Grid item xs={12} sm={6}>
                        <TextField
                            label="Last Name"
                            placeholder="Last Name"
                            className={classes.textField}
                            margin="normal"
                            value={this.state.lastName}
                            onChange={this.handleInputChange('lastName')}
                        />
                    </Grid>
                }


                    <Grid item xs={12} sm={6}>
                    {this.state.driverInfoMode == 'read'
                      ?
                        <div>
                          <InputLabel>Date of birth</InputLabel>
                          <br/>
                          <h6>{this.state.dateOfBirth}&nbsp;</h6>
                        </div>
                      :
                        <TextField
                          label="Date of birth"
                          type="date"
                          className={[classes.textField, classes.dateField]}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          value={this.state.dateOfBirth}
                          onChange={this.handleInputChange('dateOfBirth')}
                        />
                      }
                    </Grid>


                <Grid item xs={12} sm={6}>
                {this.state.driverInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Driver license</InputLabel>
                      <br/>
                      <h6>{this.state.dl_number}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        label="Driver license"
                        placeholder="Driver license"
                        className={classes.textField}
                        margin="normal"
                        value={this.state.dl_number}
                        onChange={this.handleInputChange('dl_number')}
                    />
                }
                </Grid>

                <Grid item xs={12} sm={6}>
                {this.state.driverInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Driver license Issued</InputLabel>
                      <br/>
                      <h6>{this.state.dl_issue_date}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                      label="Driver license Issued"
                      type="date"
                      margin="normal"
                      className={[classes.textField, classes.dateField]}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      value={this.state.dl_issue_date}
                      onChange={this.handleInputChange('dl_issue_date')}
                    />
                }
                </Grid>

                <Grid item xs={12} sm={6}>
                {this.state.driverInfoMode == 'read'
                  ?
                  <div>
                    <InputLabel>Driver license Expiration</InputLabel>
                    <br/>
                    <h6>{this.state.dl_expiration_date}&nbsp;</h6>
                  </div>
                  :
                    <TextField
                      label="Driver license Expiration"
                      type="date"
                      margin="normal"
                      className={classes.textField, classes.dateField}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      value={this.state.dl_expiration_date}
                      onChange={this.handleInputChange('dl_expiration_date')}
                    />
                }
                </Grid>

                <Grid item xs={12} sm={6}>
                {this.state.driverInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Street</InputLabel>
                      <br/>
                      <h6>{this.state.street}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        label="Street Address"
                        placeholder="Street Address"
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.street}
                        onChange={this.handleInputChange('street')}
                    />
                }
                </Grid>
                <Grid item xs={12} sm={6}>
                {this.state.driverInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>City</InputLabel>
                      <br/>
                      <h6>{this.state.city}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        label="City"
                        placeholder="City"
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.city}
                        onChange={this.handleInputChange('city')}
                    />
                }
                </Grid>

                <Grid item xs={12} sm={6}>
                {this.state.driverInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>State</InputLabel>
                      <br/>
                      <h6>{this.state.state}&nbsp;</h6>
                    </div>
                  :
                    <FormControl className={[classes.formControl, classes.select]}>
                      <InputLabel htmlFor="driverState">state</InputLabel>
                      <Select
                        value={this.state.state}
                        onChange={this.handleInputChange('state')}
                        inputProps={{
                          name: 'state',
                          id: 'driverState',
                        }}>
                        <MenuItem value="">Select</MenuItem>
                        <MenuItem value="AK">AK</MenuItem>
                        <MenuItem value="AL">AL</MenuItem>
                        <MenuItem value="AR">AR</MenuItem>
                        <MenuItem value="AZ">AZ</MenuItem>
                        <MenuItem value="CA">CA</MenuItem>
                        <MenuItem value="CO">CO</MenuItem>
                        <MenuItem value="CT">CT</MenuItem>
                        <MenuItem value="DC">DC</MenuItem>
                        <MenuItem value="DE">DE</MenuItem>
                        <MenuItem value="FL">FL</MenuItem>
                        <MenuItem value="GA">GA</MenuItem>
                        <MenuItem value="HI">HI</MenuItem>
                        <MenuItem value="IA">IA</MenuItem>
                        <MenuItem value="ID">ID</MenuItem>
                        <MenuItem value="IL">IL</MenuItem>
                        <MenuItem value="IN">IN</MenuItem>
                        <MenuItem value="KS">KS</MenuItem>
                        <MenuItem value="KY">KY</MenuItem>
                        <MenuItem value="LA">LA</MenuItem>
                        <MenuItem value="MA">MA</MenuItem>
                        <MenuItem value="MD">MD</MenuItem>
                        <MenuItem value="ME">ME</MenuItem>
                        <MenuItem value="MI">MI</MenuItem>
                        <MenuItem value="MN">MN</MenuItem>
                        <MenuItem value="MO">MO</MenuItem>
                        <MenuItem value="MS">MS</MenuItem>
                        <MenuItem value="MT">MT</MenuItem>
                        <MenuItem value="NC">NC</MenuItem>
                        <MenuItem value="ND">ND</MenuItem>
                        <MenuItem value="NE">NE</MenuItem>
                        <MenuItem value="NH">NH</MenuItem>
                        <MenuItem value="NJ">NJ</MenuItem>
                        <MenuItem value="NM">NM</MenuItem>
                        <MenuItem value="NV">NV</MenuItem>
                        <MenuItem value="NY">NY</MenuItem>
                        <MenuItem value="OH">OH</MenuItem>
                        <MenuItem value="OK">OK</MenuItem>
                        <MenuItem value="OR">OR</MenuItem>
                        <MenuItem value="PA">PA</MenuItem>
                        <MenuItem value="RI">RI</MenuItem>
                        <MenuItem value="SC">SC</MenuItem>
                        <MenuItem value="SD">SD</MenuItem>
                        <MenuItem value="TN">TN</MenuItem>
                        <MenuItem value="TX">TX</MenuItem>
                        <MenuItem value="UT">UT</MenuItem>
                        <MenuItem value="VA">VA</MenuItem>
                        <MenuItem value="VT">VT</MenuItem>
                        <MenuItem value="WA">WA</MenuItem>
                        <MenuItem value="WI">WI</MenuItem>
                        <MenuItem value="WV">WV</MenuItem>
                        <MenuItem value="WY">WY</MenuItem>
                      </Select>
                    </FormControl>
                  }
                </Grid>

                <Grid item xs={12} sm={6}>
                {this.state.driverInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Zip</InputLabel>
                      <br/>
                      <h6>{this.state.zipcode}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        label="Zip"
                        placeholder="zipcode"
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.zipcode}
                        onChange={this.handleInputChange('zipcode')}
                    />
                }
                </Grid>

                <Grid item xs={12} sm={6}>
                {this.state.driverInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Phone</InputLabel>
                      <br/>
                      <h6>{this.state.phone}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        label="Primary Phone Number"
                        placeholder="Primary Phone Number"
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        value={this.state.phone}
                        onChange={this.handleInputChange('phone')}
                    />
                }
                </Grid>
                <Grid item xs={12} sm={6}>
                  {this.state.driverInfoMode == 'read'
                    ?
                      <div>
                        <InputLabel>Secondary Phone</InputLabel>
                        <br/>
                        <h6>{this.state.secondaryPhone}&nbsp;</h6>
                      </div>
                    :
                      <TextField
                          id="standard-with-placeholder"
                          label="Secondary Phone Number"
                          placeholder="Secondary Phone Number"
                          className={classes.textField}
                          margin="normal"
                          fullWidth
                          value={this.state.secondaryPhone}
                          onChange={this.handleInputChange('secondaryPhone')}
                      />
                  }
                </Grid>
                <Grid item xs={12} sm={6}>
                  {this.state.driverInfoMode == 'read'
                    ?
                      <div>
                        <InputLabel>Emergency Contact Person</InputLabel>
                        <br/>
                        <h6>{this.state.emergencyContactPerson}&nbsp;</h6>
                      </div>
                    :
                      <TextField
                          id="standard-with-placeholder"
                          label="Emergency Contact Person"
                          placeholder="Emergency Contact Person"
                          className={classes.textField}
                          margin="normal"
                          fullWidth
                          value={this.state.emergencyContactPerson}
                          onChange={this.handleInputChange('emergencyContactPerson')}
                          />
                  }
                </Grid>
                <Grid item xs={12} sm={6}>
                  {this.state.driverInfoMode == 'read'
                    ?
                      <div>
                        <InputLabel>Emergency Contact Phone</InputLabel>
                        <br/>
                        <h6>{this.state.emergencyContactPhone}&nbsp;</h6>
                      </div>
                    :
                      <TextField
                          id="standard-with-placeholder"
                          label="Emergency Contact Person"
                          placeholder="Emergency Contact Person"
                          className={classes.textField}
                          margin="normal"
                          fullWidth
                          value={this.state.emergencyContactPhone}
                          onChange={this.handleInputChange('emergencyContactPhone')}
                      />
                  }
                </Grid>

                <Grid item xs={12} sm={6}>
                  {this.state.driverInfoMode == 'read'
                    ?
                      <div>
                        <InputLabel>Languages Known</InputLabel>
                        <br/>
                        <h6>{this.state.languageKnown.join(",")}&nbsp;</h6>
                      </div>
                    :
                      <FormControl className={[classes.formControl, classes.select]}>
                        <InputLabel htmlFor="languageKnown">Known Languages</InputLabel>
                        <Select
                          multiple
                          value={this.state.languageKnown}
                          onChange={this.handleInputChange('languageKnown')}
                          inputProps={{
                            name: 'languageKnown',
                            id: 'languageKnown',
                          }}>
                          <MenuItem value="Chinese">Chinese</MenuItem>
                          <MenuItem value="English">English</MenuItem>
                          <MenuItem value="Hindi">Hindi</MenuItem>
                          <MenuItem value="Nepali">Nepali</MenuItem>
                          <MenuItem value="Spanish">Spanish</MenuItem>
                        </Select>
                      </FormControl>
                  }
                </Grid>

                <Grid item xs={12} sm={6}>
                  {this.state.driverInfoMode == 'read'
                    ?
                      <div>
                        <InputLabel>Preferred Language</InputLabel>
                        <br/>
                        <h6>{this.state.languagePreference}&nbsp;</h6>
                      </div>
                    :
                      <FormControl className={[classes.formControl, classes.select]}>
                        <InputLabel htmlFor="languagePreference">Preferred Language</InputLabel>
                        <Select
                          value={this.state.languagePreference}
                          onChange={this.handleInputChange('languagePreference')}
                          inputProps={{
                            name: 'languagePreference',
                            id: 'languagePreference',
                          }}>
                          <MenuItem value="Chinese">Chinese</MenuItem>
                          <MenuItem value="English">English</MenuItem>
                          <MenuItem value="Hindi">Hindi</MenuItem>
                          <MenuItem value="Nepali">Nepali</MenuItem>
                          <MenuItem value="Spanish">Spanish</MenuItem>
                        </Select>
                      </FormControl>
                  }
                </Grid>
            </Grid>
          </div>
        )



      case 2:
        return (
          <div>
          {this.state.vehicleInfoMode != 'read'
            ?<div>
                <SaveRounded className={classes.floatRight}
                    color="primary"
                    onClick={() => this.updateLoggedProfileDetails()}>
                  <Visibility />
                </SaveRounded>
            </div>
            : null
          }
          {this.state.vehicleInfoMode == 'read' && this.state.showVehicleInfoMode
            ?
              <EditRounded className={classes.floatRight}
                color="primary"
                onClick={() => this.editVehicleInfoModeToggle()}>
                <Visibility />
              </EditRounded>
            :
              null
          }

          <h3> Vehicle Information</h3>
          <Grid container spacing={24}>
              <Grid item xs={12} sm={6}>
                {this.state.vehicleInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>License Plate Number</InputLabel>
                      <br/>
                      <h6>{this.state.carNo}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        id="standard-with-placeholder"
                        label="License Plate Number"
                        placeholder="License Plate Number"
                        className={classes.textField}
                        margin="normal"
                        value={this.state.carNo}
                        onChange={this.handleInputChange('carNo')}
                    />
                }
              </Grid>

              <Grid item xs={12} sm={6}>
                {this.state.vehicleInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Vehicle Make</InputLabel>
                      <br/>
                      <h6>{this.state.carMake}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        id="standard-with-placeholder"
                        label="Vehicle Make"
                        placeholder="Vehicle Make"
                        className={classes.textField}
                        margin="normal"
                        value={this.state.carMake}
                        onChange={this.handleInputChange('carMake')}
                    />
                }
              </Grid>

              <Grid item xs={12} sm={6}>
                {this.state.vehicleInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Vehicle Model</InputLabel>
                      <br/>
                      <h6>{this.state.carModel}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        id="standard-with-placeholder"
                        label="Vehicle Model"
                        placeholder="Vehicle Model"
                        className={classes.textField}
                        margin="normal"
                        value={this.state.carModel}
                        onChange={this.handleInputChange('carModel')}
                    />
                }
              </Grid>

              <Grid item xs={12} sm={6}>
              {this.state.vehicleInfoMode == 'read'
                ?
                  <div>
                    <InputLabel>Vehicle Year</InputLabel>
                    <br/>
                    <h6>{this.state.carYear}&nbsp;</h6>
                  </div>
                :
                  <FormControl className={[classes.formControl, classes.select]}>
                    <InputLabel htmlFor="carYear">Vehicle Year</InputLabel>
                    <Select
                      value={this.state.carYear}
                      onChange={this.handleInputChange('carYear')}
                      inputProps={{
                        name: 'carYear',
                        id: 'carYear',
                      }}>
                      <MenuItem value="">Select</MenuItem>
                      <MenuItem value="2011">2011</MenuItem>
                      <MenuItem value="2012">2012</MenuItem>
                      <MenuItem value="2013">2013</MenuItem>
                      <MenuItem value="2014">2014</MenuItem>
                      <MenuItem value="2015">2015</MenuItem>
                      <MenuItem value="2016">2016</MenuItem>
                      <MenuItem value="2017">2017</MenuItem>
                      <MenuItem value="2018">2018</MenuItem>
                      <MenuItem value="2019">2019</MenuItem>
                    </Select>
                  </FormControl>
                }
              </Grid>

              <Grid item xs={12} sm={6}>
                {this.state.vehicleInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Car Body Type</InputLabel>
                      <br/>
                      <h6>{this.state.carBodyType}&nbsp;</h6>
                    </div>
                  :
                    <FormControl className={[classes.formControl, classes.select]}>
                      <InputLabel htmlFor="carBodyType">Car Body Type</InputLabel>
                      <Select
                        value={this.state.carBodyType}
                        onChange={this.handleInputChange('carBodyType')}
                        inputProps={{
                          name: 'carBodyType',
                          id: 'carBodyType',
                        }}>
                        <MenuItem value="">Select</MenuItem>
                        <MenuItem value="Convertible">Convertible</MenuItem>
                        <MenuItem value="Coupe">Coupe</MenuItem>
                        <MenuItem value="Hatchback">Hatchback</MenuItem>
                        <MenuItem value="Jeep">Jeep</MenuItem>
                        <MenuItem value="Sedan">Sedan</MenuItem>
                        <MenuItem value="SUV">SUV</MenuItem>
                        <MenuItem value="Wagon">Wagon</MenuItem>
                        <MenuItem value="Van">Van</MenuItem>
                      </Select>
                    </FormControl>
                }
              </Grid>

              <Grid item xs={12} sm={6}>
                {this.state.vehicleInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Fuel Type</InputLabel>
                      <br/>
                      <h6>{this.state.carFuelType}&nbsp;</h6>
                    </div>
                  :
                    <TextField
                        id="standard-with-placeholder"
                        label="Fuel Type"
                        placeholder="Fuel type"
                        className={classes.textField}
                        margin="normal"
                        value={this.state.carFuelType}
                        onChange={this.handleInputChange('carFuelType')}
                    />
                }
              </Grid>

              <Grid item xs={12} sm={6}>
              {this.state.vehicleInfoMode == 'read'
                ?
                  <div>
                    <InputLabel>Number of Seats</InputLabel>
                    <br/>
                    <h6>{this.state.noOfSeat}&nbsp;</h6>
                  </div>
                :
                  <FormControl className={[classes.formControl, classes.select]}>
                    <InputLabel htmlFor="noOfSeat">Number of Seats</InputLabel>
                    <Select
                      value={this.state.noOfSeat}
                      onChange={this.handleInputChange('noOfSeat')}
                      inputProps={{
                        name: 'noOfSeat',
                        id: 'noOfSeat',
                      }}>
                      <MenuItem value="">Select</MenuItem>
                      <MenuItem value="2">2</MenuItem>
                      <MenuItem value="3">3</MenuItem>
                      <MenuItem value="4">4</MenuItem>
                      <MenuItem value="5">5</MenuItem>
                      <MenuItem value="6">6</MenuItem>
                      <MenuItem value="7">7</MenuItem>
                      <MenuItem value="8">8</MenuItem>
                      <MenuItem value="9">9</MenuItem>
                      <MenuItem value="10">10</MenuItem>
                    </Select>
                  </FormControl>
                }
              </Grid>
              <Grid item xs={12} sm={6}>
              {this.state.vehicleInfoMode == 'read'
                ?
                  <div>
                    <InputLabel>Color</InputLabel>
                    <br/>
                    <h6>{this.state.carColor}&nbsp;</h6>
                  </div>
                :
                  <TextField
                      id="standard-with-placeholder"
                      label="Color"
                      placeholder="Color"
                      className={classes.textField}
                      margin="normal"
                      value={this.state.carColor}
                      onChange={this.handleInputChange('carColor')}
                  />
              }
              </Grid>

              <Grid item xs={12} sm={6}>
              {this.state.vehicleInfoMode == 'read'
                ?
                  <div>
                    <InputLabel>Vin Number</InputLabel>
                    <br/>
                    <h6>{this.state.carVinNumber}&nbsp;</h6>
                  </div>
                :
                  <TextField
                      id="standard-with-placeholder"
                      label="Vin Number"
                      placeholder="Vin Number"
                      className={classes.textField}
                      margin="normal"
                      value={this.state.carVinNumber}
                      onChange={this.handleInputChange('carVinNumber')}
                  />
                }
              </Grid>
              <Grid item xs={12} sm={6}>
                {this.state.vehicleInfoMode == 'read'
                  ?
                    <div>
                      <InputLabel>Registration State</InputLabel>
                      <br/>
                      <h6>{this.state.carRegistrationState}&nbsp;</h6>
                    </div>
                  :
                    <FormControl className={[classes.formControl, classes.select]}>
                      <InputLabel htmlFor="carRegistrationState">state</InputLabel>
                      <Select
                        value={this.state.carRegistrationState}
                        onChange={this.handleInputChange('carRegistrationState')}
                        inputProps={{
                          name: 'carRegistrationState',
                          id: 'carRegistrationState',
                        }}>
                        <MenuItem value="">Select</MenuItem>
                        <MenuItem value="AK">AK</MenuItem>
                        <MenuItem value="AL">AL</MenuItem>
                        <MenuItem value="AR">AR</MenuItem>
                        <MenuItem value="AZ">AZ</MenuItem>
                        <MenuItem value="CA">CA</MenuItem>
                        <MenuItem value="CO">CO</MenuItem>
                        <MenuItem value="CT">CT</MenuItem>
                        <MenuItem value="DC">DC</MenuItem>
                        <MenuItem value="DE">DE</MenuItem>
                        <MenuItem value="FL">FL</MenuItem>
                        <MenuItem value="GA">GA</MenuItem>
                        <MenuItem value="HI">HI</MenuItem>
                        <MenuItem value="IA">IA</MenuItem>
                        <MenuItem value="ID">ID</MenuItem>
                        <MenuItem value="IL">IL</MenuItem>
                        <MenuItem value="IN">IN</MenuItem>
                        <MenuItem value="KS">KS</MenuItem>
                        <MenuItem value="KY">KY</MenuItem>
                        <MenuItem value="LA">LA</MenuItem>
                        <MenuItem value="MA">MA</MenuItem>
                        <MenuItem value="MD">MD</MenuItem>
                        <MenuItem value="ME">ME</MenuItem>
                        <MenuItem value="MI">MI</MenuItem>
                        <MenuItem value="MN">MN</MenuItem>
                        <MenuItem value="MO">MO</MenuItem>
                        <MenuItem value="MS">MS</MenuItem>
                        <MenuItem value="MT">MT</MenuItem>
                        <MenuItem value="NC">NC</MenuItem>
                        <MenuItem value="ND">ND</MenuItem>
                        <MenuItem value="NE">NE</MenuItem>
                        <MenuItem value="NH">NH</MenuItem>
                        <MenuItem value="NJ">NJ</MenuItem>
                        <MenuItem value="NM">NM</MenuItem>
                        <MenuItem value="NV">NV</MenuItem>
                        <MenuItem value="NY">NY</MenuItem>
                        <MenuItem value="OH">OH</MenuItem>
                        <MenuItem value="OK">OK</MenuItem>
                        <MenuItem value="OR">OR</MenuItem>
                        <MenuItem value="PA">PA</MenuItem>
                        <MenuItem value="RI">RI</MenuItem>
                        <MenuItem value="SC">SC</MenuItem>
                        <MenuItem value="SD">SD</MenuItem>
                        <MenuItem value="TN">TN</MenuItem>
                        <MenuItem value="TX">TX</MenuItem>
                        <MenuItem value="UT">UT</MenuItem>
                        <MenuItem value="VA">VA</MenuItem>
                        <MenuItem value="VT">VT</MenuItem>
                        <MenuItem value="WA">WA</MenuItem>
                        <MenuItem value="WI">WI</MenuItem>
                        <MenuItem value="WV">WV</MenuItem>
                        <MenuItem value="WY">WY</MenuItem>
                      </Select>
                    </FormControl>
                }
              </Grid>
              <Grid item xs={12} sm={6}>
                {this.state.vehicleInfoMode == 'read' || true
                  ?
                    null
                  :
                    <label htmlFor="contained-button-file">
                        <Button variant="contained" component="span" className={classes.button}>
                            Upload Car Image
                        </Button>
                    </label>
                  }
              </Grid>
            </Grid>
          </div>
        );



      case 3:
        return (
          <div>
          {this.state.accountInfoMode != 'read'
            ?<div>
              <SaveRounded className={classes.floatRight}
                  color="primary"
                  onClick={() => this.saveAccountInfo()}>
                <Visibility />
              </SaveRounded>
            </div>
            : null
          }
          {this.state.accountInfoMode == 'read' && this.state.showAccountInfoMode
            ?
              <EditRounded className={classes.floatRight}
                color="primary"
                onClick={() => this.editAccountInfoModeToggle()}>
                <Visibility />
              </EditRounded>
            :
              null
          }
          <h3> Bank Information</h3>
          <Grid container spacing={24}>
              <Grid item xs={12} sm={6}>
              {this.state.accountInfoMode == 'read'
                ?
                  <div>
                    <InputLabel>Bank Name</InputLabel>
                    <br/>
                    <h6>{this.state.bankName}&nbsp;</h6>
                  </div>
                :
                  <TextField
                      id="standard-with-placeholder"
                      label="Bank Name"
                      placeholder="Bank Name"
                      className={classes.textField}
                      margin="normal"
                      fullWidth
                      value={this.state.bankName}
                      onChange={this.handleInputChange('bankName')}
                  />
                }
              </Grid>
              <Grid item xs={12} sm={6}>
              </Grid>
              <Grid item xs={12} sm={6}>
              {this.state.accountInfoMode == 'read'
                ?
                  <div>
                    <InputLabel>Account Number</InputLabel>
                    <br/>
                    <h6>{this.state.accountNumber}&nbsp;</h6>
                  </div>
                :
                  <TextField
                      id="standard-with-placeholder"
                      label="Bank Account No"
                      placeholder="Your bank Account No"
                      className={classes.textField}
                      margin="normal"
                      fullWidth
                      value={this.state.accountNumber}
                      onChange={this.handleInputChange('accountNumber')}
                  />
                }
              </Grid>
              <Grid item xs={12} sm={6}>
              </Grid>
              <Grid item xs={12} sm={6}>
              {this.state.accountInfoMode == 'read'
                ?
                  <div>
                    <InputLabel>Routing Number</InputLabel>
                    <br/>
                    <h6>{this.state.routingNumber}&nbsp;</h6>
                  </div>
                :
                  <TextField
                      id="standard-with-placeholder"
                      label="Routing Number"
                      placeholder="Routing Number"
                      className={classes.textField}
                      margin="normal"
                      fullWidth
                      value={this.state.routingNumber}
                      onChange={this.handleInputChange('routingNumber')}
                  />
                }
              </Grid>
              <Grid item xs={12} sm={6}>
              </Grid>
              <Grid item xs={12} sm={6}>
              {this.state.accountInfoMode == 'read'
                ?
                  <div>
                    <InputLabel>Social Security Number</InputLabel>
                    <br/>
                    <h6>{this.state.ssnNumber}&nbsp;</h6>
                  </div>
                :
                  <TextField
                      id="standard-with-placeholder"
                      label="Social Security Number"
                      placeholder="Social Security Number"
                      className={classes.textField}
                      margin="normal"
                      fullWidth
                      value={this.state.ssnNumber}
                      onChange={this.handleInputChange('ssnNumber')}
                  />
                }
              </Grid>
              <Grid item xs={12} sm={6}>
              </Grid>
            </Grid>
          </div>
        )

      default:
        return 'Unknown stepIndex';
    }
  }

  toggleModes(activeMode) {
    let modes = ["showLoginMode", "showDriverInfoMode", "showVehicleInfoMode", "showAccountInfoMode"];

    for(var i = 0; i < modes.length; i++){
      let mode = modes[i];
      let modeValue = !activeMode || (mode == activeMode);
      this.setState({ [mode]: modeValue });
    }

    if(!activeMode){
      let displayModes = ["loginMode", "driverInfoMode", "vehicleInfoMode", "accountInfoMode"];
      for(var i = 0; i < displayModes.length; i++){
        let mode = displayModes[i];
        this.setState({ [mode]: "read" });
      }
    }
  }

  handleShowAnswer = () => {
      this.setState(state => ({ showAnswer: !state.showAnswer }));
  };

  updatedProfileDetails(){
    this.toggleModes(null);
  }

  setProfileDetails(data) {
    data = JSON.parse(data);
    console.log(data);

    this.setState(prevState => (
      {
        initialState: data,
        accountNumber: data.accountNumber || '',
        bankName: data.bankName || '',
        carBackImageUrl: data.carBackImageUrl || '',
        carBodyType: data.carBodyType || '',
        carColor: data.carColor || '',
        carFrontImageUrl: data.carFrontImageUrl || '',
        carFuelType: data.carFuelType || '',
        carMake: data.carMake || '',
        carModel: data.carModel || '',
        carNo: data.carNo || '',
        carRegistrationState: data.carRegistrationState || '',
        carVinNumber: data.carVinNumber || '',
        carYear: data.carYear || '',
        city: data.city || '',
        dateOfBirth: data.dateOfBirth || '',
        deviceId: "N/A",
        dl_expiration_date: data.dl_expiration_date || '',
        dl_issue_date: data.dl_issue_date || '',
        dl_number: data.dl_number || '',
        driverId: data.driverId || '',
        driverImageUrl: data.driverImageUrl || '',
        email: data.email || '',
        firstName: data.firstName || '',
        emergencyContactPerson: data.emergencyContactPerson || '',
        emergencyContactPhone: data.emergencyContactPhone || '',
        isEULAAccepted: data.isEULAAccepted || '',
        isPasswordRecoveryUsingText: 0,
        languageKnown: data.languageKnown == null ? [] : data.languageKnown,
        languagePreference: data.languagePreference || '',
        lastName: data.lastName || '',
        password_recovery_a1: data.password_recovery_a1 || '',
        password_recovery_q1: data.password_recovery_q1 || '',
        phone: data.phone || '',
        routingNumber: data.routingNumber || '',
        secondaryPhone: data.secondaryPhone || '',
        ssnNumber: data.ssnNumber || '',
        state: data.state || '',
        street: data.street || '',
        triverName: data.triverName || '',
        userId: data.driverId || '',
        userName: data.userName || '',
        zipcode: data.zipcode || '',

        logged: true,
      }));

      if(!data.isEULAAccepted) {
        this.toggleEULA(true);
      }
  }

  handleEULAAccept() {
    let self = this;
    self.toggleEULA(false);
    self.setState(prevState => (
    {
      isEULAAccepted: 1,
    }));

    setTimeout(function(){
        self.updateLoggedProfileDetails();
    }, 100)
  }

  toggleEULA(show) {
    this.setState(prevState => (
    {
      showEULA: show,
    }));
  }

  toggleLoading(loading) {
    this.setState(prevState => (
    {
      isLoading: loading,
    }));
  }

  setInitialState() {
    this.setState(prevState => (
    {
      mode: 'read',
    }));

    var data = this.state.initialState;
    if(data == null)
      return;

    this.setState(prevState => (
    {
      accountNumber: data.accountNumber || '',
      bankName: data.bankName || '',
      carBackImageUrl: data.carBackImageUrl || '',
      carBodyType: data.carBodyType || '',
      carColor: data.carColor || '',
      carFrontImageUrl: data.carFrontImageUrl || '',
      carFuelType: data.carFuelType || '',
      carMake: data.carMake || '',
      carModel: data.carModel || '',
      carNo: data.carNo || '',
      carRegistrationState: data.carRegistrationState || '',
      carVinNumber: data.carVinNumber || '',
      carYear: data.carYear || '',
      city: data.city || '',
      dateOfBirth: data.dateOfBirth || '',
      deviceId: "N/A",
      dl_expiration_date: data.dl_expiration_date || '',
      dl_issue_date: data.dl_issue_date || '',
      dl_number: data.dl_number || '',
      driverId: data.driverId || '',
      driverImageUrl: data.driverImageUrl || '',
      email: data.email || '',
      firstName: data.firstName || '',
      emergencyContactPerson: data.emergencyContactPerson || '',
      emergencyContactPhone: data.emergencyContactPhone || '',
      isEULAAccepted: data.isEULAAccepted || '',
      isPasswordRecoveryUsingText: data.isPasswordRecoveryUsingText || '',
      languageKnown: data.languageKnown == null ? [] : data.languageKnown,
      languagePreference: data.languagePreference || '',
      lastName: data.lastName || '',
      password_recovery_a1: data.password_recovery_a1 || '',
      password_recovery_q1: data.password_recovery_q1 || '',
      phone: data.phone || '',
      routingNumber: data.routingNumber || '',
      secondaryPhone: data.secondaryPhone || '',
      ssnNumber: data.ssnNumber || '',
      state: data.state || '',
      street: data.street || '',
      triverName: data.triverName || '',
      userId: data.driverId || '',
      userName: data.userName || '',
      zipcode: data.zipcode || '',

      logged: true
    }));
  }

  getCurrentUserInfo(userid) {
    let data = this.state;
    return {
        accountNumber: data.accountNumber,
        bankName: data.bankName,
        carBackImageUrl: data.carBackImageUrl,
        carBodyType: data.carBodyType,
        carColor: data.carColor,
        carFrontImageUrl: data.carFrontImageUrl,
        carFuelType: data.carFuelType,
        carMake: data.carMake,
        carModel: data.carModel,
        carNo: data.carNo,
        carRegistrationState: data.carRegistrationState,
        carVinNumber: data.carVinNumber,
        carYear: data.carYear,
        city: data.city,
        dateOfBirth: data.dateOfBirth,
        deviceId: "N/A",
        dl_expiration_date: data.dl_expiration_date,
        dl_issue_date: data.dl_issue_date,
        dl_number: data.dl_number,
        driver_id: userid,
        driverImageUrl: data.driverImageUrl,
        email: data.email,
        firstName: data.firstName,
        emergencyContactPerson: data.emergencyContactPerson,
        emergencyContactPhone: data.emergencyContactPhone,
        isEULAAccepted: data.isEULAAccepted,
        isPasswordRecoveryUsingText: 0,
        languageKnown: !data.languageKnown || data.languageKnown.length == 0 ? null : data.languageKnown,
        languagePreference: data.languagePreference,
        lastName: data.lastName,
        password_recovery_q1: data.password_recovery_q1,
        password_recovery_a1: data.password_recovery_a1,
        phone: data.phone,
        routingNumber: data.routingNumber,
        secondaryPhone: data.secondaryPhone,
        ssnNumber: data.ssnNumber,
        state: data.state,
        street: data.street,
        triverName: data.triverName,
        userId: userid,
        userName: data.userName,
        zipcode: data.zipcode,
      }
  }

  editLoginModeToggle() {
    this.setState(prevState => (
    {
      loginMode: this.state.loginMode == 'read' ? 'edit' : 'read'
    }));
    if(this.state.loginMode == 'read'){
      this.toggleModes("showLoginMode");
    }
    else{
      this.toggleModes(null);
    }
  }

  editDriverInfoModeToggle() {
    this.setState(prevState => (
    {
      driverInfoMode: this.state.driverInfoMode == 'read' ? 'edit' : 'read'
    }));
    if(this.state.driverInfoMode == 'read'){
      this.toggleModes("showDriverInfoMode");
    }
    else{
      this.toggleModes(null);
    }
  }

  editVehicleInfoModeToggle() {
    this.setState(prevState => (
    {
      vehicleInfoMode: this.state.vehicleInfoMode == 'read' ? 'edit' : 'read'
    }));
    if(this.state.vehicleInfoMode == 'read'){
      this.toggleModes("showVehicleInfoMode");
    }
    else{
      this.toggleModes(null);
    }
  }

  editAccountInfoModeToggle(){
    this.setState(prevState => (
    {
      accountInfoMode: this.state.accountInfoMode == 'read' ? 'edit' : 'read'
    }));
    if(this.state.accountInfoMode == 'read'){
      this.toggleModes("showAccountInfoMode");
    }
    else{
      this.toggleModes(null);
    }
  }

  componentDidMount() {
    // var userinfo = this.props.getLoggedUserInfo();
    // if(userinfo.userid && userinfo.token){
    //   this.getLoggedProfileDetails(userinfo);
    // }
  }

  handleNext = () => {
    if(this.state.activeStep === this.getSteps().length - 1){
      this.props.handleProfileComplete();
    }
    else{
     this.setState(state => ({
       activeStep: state.activeStep + 1,
     }));
   }
 };

 handleBack = () => {
   this.setState(state => ({
     activeStep: state.activeStep - 1,
   }));
 };

  handleInputChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  getLoggedProfileDetails(userinfo){
    //userinfo = JSON.parse(userinfo);
    var userid = userinfo.userid;
    var token = userinfo.token;
    let url = this.state.driverInfoURL;
    let body = {
      "userId": userid
    }
    let options = {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "cache-control": "no-cache",
        "Authorization": "Bearer " + token
      },
      body: JSON.stringify(body)
    }
    this.ajaxCall(url, options, this.setProfileDetails)
  }

  updateLoggedProfileDetails(){
    var userinfo = this.props.getLoggedUserInfo();
    if(userinfo.userid && userinfo.token){
      var userid = userinfo.userid;
      var token = userinfo.token;
      let url = this.state.updateLoggedProfileURL;
      let body = this.getCurrentUserInfo(userid);

      let options = {
        method: 'POST',
        headers: {
          "Content-Type": "application/json",
          "cache-control": "no-cache",
          "Authorization": "Bearer " + token
        },
        body: JSON.stringify(body)
      }
      this.ajaxCall(url, options, this.updatedProfileDetails);
    }
  }

  saveAccountInfo() {
    var userinfo = this.props.getLoggedUserInfo();
    if(userinfo.userid && userinfo.token){
      var userid = userinfo.userid;
      var token = userinfo.token;
      let url = this.state.driverPaymentURL;
      let body = this.getCurrentUserInfo(userid);

      let options = {
        method: 'POST',
        headers: {
          "Content-Type": "application/json",
          "cache-control": "no-cache",
          "Authorization": "Bearer " + token
        },
        body: JSON.stringify(body)
      }
      this.ajaxCall(url, options, this.setProfileDetails)
    }
  }

  ajaxCall = (url, options, callback) => {
    let self = this;
    self.toggleLoading(true);
    fetch(url, options)
        .then(function (response) {
        return response.json();
    })
    .then(function (result) {
        if(!result.error){
          callback(result);
        }
        else {
          alert('error signing in');
          //self.displayError();
        }
        self.toggleLoading(false);
    })
    .catch (function (error) {
      self.toggleLoading(false);
      console.log(error);
      //self.displayError();
    });
  }

  render() {
    const { classes } = this.props;
    const steps = this.getSteps();
    const { activeStep } = this.state;

    return (
        <div className={classes.background}>
            <div className="container">

                <h2 className={classes.pageTitle}>Driver's Profile</h2>

                <Paper style={{ padding: 20 }}>
                    <Paper style={{ margin: 30, padding: 30 }}>
                      <div className={classes.root}>
                        <Stepper activeStep={activeStep} alternativeLabel>
                          {steps.map(label => {
                            return (
                              <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                              </Step>
                            );
                          })}
                        </Stepper>
                        <div>
                          <br/>
                            <div>
                              <div>
                                {this.getStepContent(activeStep)}
                              </div>
                              <div>
                                <br/>
                                <Button
                                  disabled={activeStep === 0 || !this.state.showLoginMode
                                                             || !this.state.showDriverInfoMode
                                                             || !this.state.showVehicleInfoMode
                                                             || !this.state.showAccountInfoMode}
                                  onClick={this.handleBack}
                                  className={classes.backButton}
                                >
                                  Back
                                </Button>
                                <Button variant="contained" color="primary" onClick={this.handleNext}
                                  disabled={!this.state.showLoginMode
                                            || !this.state.showDriverInfoMode
                                            || !this.state.showVehicleInfoMode
                                            || !this.state.showAccountInfoMode}
                                >
                                  {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                </Button>
                              </div>
                            </div>
                        </div>
                      </div>
                    </Paper>
                </Paper>
            </div>

          <Dialog
            //fullScreen={fullScreen}
            open={this.state.showEULA}
            aria-labelledby="responsive-dialog-title">
            <DialogTitle id="responsive-dialog-title">{"End User License Agreement"}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                What is Lorem Ipsum?
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

                Why do we use it?
                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


                Where does it come from?
                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

                The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.

                Where can I get some?
                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleEULAAccept} color="primary" autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>

          <Dialog
            open={this.state.isLoading}
            aria-labelledby="responsive-dialog-title">
            <DialogContent>
              <CircularProgress />
            </DialogContent>
          </Dialog>
        </div>
    )
  }
};

// export default profile
// export default withStyles(styles)(Profile);
export default withRouter( withStyles(styles)(Profile));
