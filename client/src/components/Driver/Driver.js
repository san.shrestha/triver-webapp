import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Faq from './Faq';
import Profile from './Profile';
import Referral from './Referral';
import TourHistory from './TourHistory';
import Earnings from './Earnings';
import querySearch from 'stringquery'

const styles = theme => ({

    root: {
        flexGrow: 1,
        paddingTop: 20
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    background: {
        backgroundColor: '#e6e6e6',
        paddingTop: 20,
        paddingBottom: 120,
        // display: 'none'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: 20
    },
    button: {
        height: 10,
        marginTop: 10
    },
    tabWrpper: {
        backgroundColor: 'black'
    },
    label: {
        color: 'white',
        lineHeight: 6
    }
});

class Driver extends React.Component {

    constructor(props) {
        super(props);

        this.handleProfileComplete = this.handleProfileComplete.bind(this);
    }

    state = {
        value: 0,
    };

    componentDidMount() {
        /*const query = querySearch(this.props.location.search);
        const idx = Number(query.nav);
        if(idx){
          this.setState({value : idx});
        }*/
      }

    handleChange = (event, value) => {
        if(value === 3){
          this.props.signOutHandler();
        }
        else{
          this.setState({ value });
        }
    };

    handleProfileComplete(){
      this.setState({value : 1});
    }

    render() {
        const { classes } = this.props;
        const { value } = this.state;

        document.body.classList.add('innerPage');
        return (
            <Paper square>
                <Tabs
                    value={this.state.value}
                    indicatorColor="primary"
                    textColor="secondary"
                    onChange={this.handleChange}
                    centered
                    className={classes.tabWrpper}
                >
                    <Tab className={classes.label} label="Profile" />
                    <Tab className={classes.label} label="Earnings" />
                    <Tab className={classes.label} label="Tour History" />
                    <Tab className={classes.label} label="Exit" />
                    {/* <Tab className={classes.label} label="FAQ" /> */}
                </Tabs>
                {value === 0 && <Profile getLoggedUserInfo={this.props.getLoggedUserInfo}
                                         handleProfileComplete={this.handleProfileComplete}
                                         classes={classes}>Item One</Profile>}
                {value === 1 && <Earnings classes={classes}>Item Two</Earnings>}
                {value === 2 && <TourHistory classes={classes}>Item Three</TourHistory>}
                {/*{value === 4 && <Faq classes={classes}>Item Three</Faq>}*/}
                {value === 3}
            </Paper>
        );
    }
}

// export default Driver;
export default withStyles(styles)(Driver);
