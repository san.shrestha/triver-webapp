import React, { Component } from 'react';
import logo from './logo.svg';
import { BrowserRouter } from 'react-router-dom'
import NavBar from './components/Navbar/Navbar'
import './App.css';
import './components/Navbar/Navbar';
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Signin from "./components/Signin/Signin";
import Rider from "./components/Rider/Rider";
import Driver from "./components/Driver/Driver";
import Team from "./components/Team/Team";

const Main = (props) => {
  return (
      <div style={{marginTop:40}}>hiii</div>
  );
};

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      usertype:'',
      userid: '',
      username: '',
      email: '',
      token: '',
      logged: false,
      key:'userinfo',
      home: '/'
    }
    this.getLoggedUserInfo = this.getLoggedUserInfo.bind(this);
    this.updateUserInfo = this.updateUserInfo.bind(this);
    this.signOutHandler = this.signOutHandler.bind(this);
  }

  componentDidMount = () => {
    var userinfo = window.sessionStorage.getItem(this.state.key);
    if(userinfo) {
      let userinfoJSON = JSON.parse(userinfo);
      let usertype = userinfoJSON.usertype;
      let userid = userinfoJSON.userid;
      let token =  userinfoJSON.token;
      let username = userinfoJSON.lastname;
      let email = userinfoJSON.email;
      this.setState(prevState => ({
        usertype: usertype,
        userid: userid,
        username: username,
        email: email,
        token: token,
        logged: true,
      }));
    }
  }

  updateUserInfo = (userid, token, usertype, email, username) => {
    if(userid && token){
      this.setState(prevState => ({
        usertype: usertype,
        userid: userid,
        username: username,
        email: email,
        logged: true,
        token: token
      }));
      var userinfo =  JSON.stringify({'usertype': usertype,
                                      'userid': userid,
                                      'username': username,
                                      'email': email,
                                      'token': token});

      window.sessionStorage.setItem(this.state.key, userinfo);
    }
  }

  getLoggedUserInfo = () => {
    var userinfo = window.sessionStorage.getItem(this.state.key);


    debugger;
    if(userinfo){
      var info = JSON.parse(userinfo);
      info.logged = true;
      return info;
    }
    else{
      return {
        usertype:'',
        userid: '',
        username: '',
        email: '',
        token: '',
        logged: false,
      }
    }
  }

  signOutHandler = (e) => {
    if(!this.state.logged){
      return;
    }

    this.setState(prevState => ({
      usertype: '',
      username: '',
      userid: '',
      token: '',
      logged: false,
    }));

    window.sessionStorage.removeItem(this.state.key);

    window.location.href = this.state.home;
  }

  render() {
    return (
      <div>
        <div>
        <HashRouter>
            <div>
                <NavBar getLoggedUserInfo={this.getLoggedUserInfo} signOutHandler={this.signOutHandler}></NavBar>
                <div className="content">
                  <Route
                    path="/Signup"
                    render={(props) => <Signin {...props}
                            getLoggedUserInfo={this.getLoggedUserInfo}
                            updateUserInfo={this.updateUserInfo} />}
                            />
                  <Route
                    path="/Driver"
                    render={(props) => <Driver {...props}
                            getLoggedUserInfo={this.getLoggedUserInfo}
                            signOutHandler = {this.signOutHandler} />}
                    />
                  <Route
                    path="/Rider"
                    render={(props) => <Rider {...props}
                            getLoggedUserInfo={this.getLoggedUserInfo}
                            signOutHandler = {this.signOutHandler} />}
                    />
                    <Route
                      path="/Team"
                      render={(props) => <Team {...props}
                              getLoggedUserInfo={this.getLoggedUserInfo}
                              signOutHandler = {this.signOutHandler} />}
                      />
                </div>

            </div>

        </HashRouter>
        </div>

      </div>
    );
  }
}

export default App;
