import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import './../../App.css';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: 30,
        textAlign: 'center',
        color: 'red',
    },
    pageTitle: {
        paddingTop: 10,
        paddingBottom: 20
    }
});
const partners = (props) => {

    const { classes } = props;

    return (

        <div className={classes.background}>
            <div className="container">
                <h2 className={classes.pageTitle}>Partners</h2>
                <Paper className={classes.root}>
                  <Grid container spacing={24} style={{ paddingLeft: 30, paddingRight: 30 }}>
                    <Grid item xs={12} sm={12}>
                      <section id="boardOfDirectors" class="mbr-section article mbr-parallax-background">
                        <div class="mbr-overlay">
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-md-offset-0 text-xs-center">
                                    <div class="lead">
                                        <div class="col-md-6">
                                            <br />
                                            <h2>CEO</h2>
                                            <h3>John Doe</h3>
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQMW5XEbxz082f1Q46B88ywrOs_hM9dpF7LFL9q98C5NgYUFTdQA" />
                                            <p>
                                                I am from Dayton Ohio. I am here in Sanfransisco for a week long conference.
                                                My meeting is at 9:00 - 5 pm. I would like to explore Sanfransisco after my
                                                off hours. I need your help
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <br />
                                            <h2>CTO</h2>
                                            <h3>John Doe</h3>
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQMW5XEbxz082f1Q46B88ywrOs_hM9dpF7LFL9q98C5NgYUFTdQA" />
                                            <p>
                                                I am from Dayton Ohio. I am here in Sanfransisco for a week long conference.
                                                My meeting is at 9:00 - 5 pm. I would like to explore Sanfransisco after my
                                                off hours. I need your help
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <br />
                                            <h2>CFO</h2>
                                            <h3>John Doe</h3>
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQMW5XEbxz082f1Q46B88ywrOs_hM9dpF7LFL9q98C5NgYUFTdQA" />
                                            <br />
                                            <p>
                                                I am from Dayton Ohio. I am here in Sanfransisco for a week long conference.
                                                My meeting is at 9:00 - 5 pm. I would like to explore Sanfransisco after my
                                                off hours. I need your help
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <br />
                                            <h2>TCO</h2>
                                            <h3>John Doe</h3>
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQMW5XEbxz082f1Q46B88ywrOs_hM9dpF7LFL9q98C5NgYUFTdQA" />
                                            <br />
                                            <p>
                                                I am Triver Coordinator Officer(TCO). I am here in Sanfransisco for a week long conference.
                                                My meeting is at 9:00 - 5 pm. I would like to explore Sanfransisco after my
                                                off hours. I need your help
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </section>
                    </Grid>
                  </Grid>
                </Paper>
            </div>
        </div >

    )
};

// export default referral
export default withStyles(styles)(partners);
