const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const port = process.env.PORT || 5000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var request = require('request');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/api/auth', (req, res) => {
  request({
    url:'http://localhost:8080/auth',
    method: 'POST',
    json: req.body
  }, function(error, response, body){
    if (!error && response.statusCode == 200) { 
      res.json(body); 
     }

    if(error){
      console.log('error');
    }
  })
});

if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'client/build')));
  // Handle React routing, return all requests to React app


  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
  app.get('/index1.html', function(req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index1.html'));
  });

}


app.listen(port, () => console.log(`Listening on port ${port}`));
