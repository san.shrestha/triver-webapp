import React from 'react';
import './../../App.css';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { ReactComponent as LogoSvg } from '../../assets/images/TBH_Web_logo2.svg'

const styles = theme => ({
    link: {
        cursor: 'pointer'
    }
});
class NavBar extends React.Component {

    constructor(props) {
        super(props);
        this.showLogin = this.showLogin.bind(this);

        this.state = {
            logged: false,
        }
    }

    componentDidMount(){
      let signInUser = this.props.getLoggedUserInfo();
      if(signInUser.userid && signInUser.token){
          this.setState({ logged: true });
      }
    }

    showLogin = (evt) => {
        evt.preventDefault();

        let signInUser = this.props.getLoggedUserInfo();
        console.log(signInUser);

        if(signInUser.userid && signInUser.token){
            this.setState({ logged: true });
        }

        if(evt && (evt.target.innerText === 'Sign in' || evt.target.innerText === 'My Info')){
            if(this.state.logged){
              this.props.history.push('/' + signInUser.usertype);
            }
            else{
              this.props.history.push('/Signup');
            }
            document.getElementById('mainContainer').classList.add('signIn');
        }
        else if(evt && evt.target.innerText === 'Triver Team'){
            this.props.history.push('/Team');
            // this.props.histoy.push('/Signup');
            document.getElementById('mainContainer').classList.add('signIn');
        }
        else{
            document.getElementById('mainContainer').classList.remove('signIn');
       }
   }

    render() {
        const { classes } = this.props;
        return (
        <section className="menu-0" id="menu-0">
            <nav className="navbar navbar-dropdown bg-color transparent navbar-fixed-top">
                <div className="container">

                    <div className="mbr-table">
                        <div className="mbr-table-cell">
                            <div className="navbar-brand">
                            <a className= {classes.link + " nav-link link"}  href="index.html#header1-1">
                            <LogoSvg/>
                            </a>
                            </div>

                        </div>
                        <div className="mbr-table-cell">

                            <button className="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse"
                                data-target="#exCollapsingNavbar">
                                <div className="hamburger-icon"></div>
                            </button>

                            <ul
                                onClick={this.showLogin}
                                className="nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm" id="exCollapsingNavbar">

                                <li className="nav-item dropdown">
                                    <a className="nav-link link" href="index.html#msg-box8-1">About Us</a></li>
                                <li className="nav-item">
                                    <a className="nav-link link" dataid="DriverLinkMain" href="index.html#DriverLinkMain">Triver</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link link" dataid="RiderLinkMain" href="index.html#RiderLinkMain">Rider</a>
                                </li>
                                <li className="nav-item" id="SignupLink" >
                                    <a className= {classes.link + " nav-link link"}  dataid="RiderLinkMain" >Triver Team
                                    </a>
                                </li>
                                {
                                  this.state.logged
                                    ?
                                      <li className="nav-item" id="SignupLink" >
                                          <a className= {classes.link + " nav-link link"}  dataid="RiderLinkMain" >My Info</a>
                                      </li>
                                    : null
                                }
                                {
                                  this.state.logged
                                    ? <li className="nav-item" id="SignupLink" onClick={() => this.props.signOutHandler()}>
                                          <a className= {classes.link + " nav-link link"}  dataid="RiderLinkMain" >SignOut</a>
                                      </li>

                                    : <li className="nav-item" id="SignupLink" >
                                          <a className= {classes.link + " nav-link link"}  dataid="RiderLinkMain" >Sign in</a>
                                      </li>
                                }
                            </ul>

                            <button className="navbar-toggler navbar-close collapsed" type="button" data-toggle="collapse"
                                data-target="#exCollapsingNavbar">
                                <div className="close-icon"></div>
                            </button>

                        </div>
                    </div>

                </div>
            </nav>

        </section>
        );
    }
}
export default withRouter( withStyles(styles)(NavBar));
// export default NavBar;
