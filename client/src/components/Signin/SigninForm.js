import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import './../../App.css';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import CircularProgress from '@material-ui/core/CircularProgress';
//import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import Snackbar from '@material-ui/core/Snackbar';
import Validator from 'validator';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';


const styles = theme => ({
  formControl: {
    minWidth: 225,
  },
  root: {
    flexGrow: 1,
    paddingTop: 20,
    paddingBottom: 20,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  background: {
    backgroundColor: '#e6e6e6',
    // display: 'none'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: 20
  },
  button: {
    height: 10,
    marginTop: 10
  },
  action: {
    cursor: 'pointer'
  }
});

class SigninForm extends React.Component {

  constructor(props) {
    super(props);

    this.handleErrorMessageClose = this.handleErrorMessageClose.bind(this);
  }

  state = {
    /*
    amount: '',
    passwordDriver: '',
    passwordRider: '',
    weight: '',
    weightRange: '',
    showPasswordDriver: false,
    showPasswordRider: false,
    userNameDriver: '',
    userNameRider:'',
    */

    username: '',
    email: '',
    password: '',
    confirmPassword: '',
    showPassword: false,
    showconfirmPassword: false,
    //phone: '',
    //firstname: '',
    //lastname: '',
    //middlename: '',

    enabled: true,
    authorities: null,
    online: false,
    lastPasswordResetDate: null,

    signUpMode: false,
    usertype: '',
    errorSigning: false,

    usertypeValidation: '',
    usernameValidation: '',
    emailValidation: '',
    passwordValidation: '',
    confirmPasswordValidation: '',
    //phoneValidation: '',
    //firstnameValidation: '',
    //lastnameValidation: '',
    //middlenameValidation: '',

    labelWidth: 160,

    isLoading: false,
    signInExistingURL: "/api/auth",
    signUpNewURL: "/api/signup",
  };

  componentDidMount() {
    /*
    if(!this.InputLabelRef)
      return;
    this.setState({
      labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
    });
    */
  }

  validate(field) {
    var isValid = true;
    var self = this;

    if (!field) {
      self.setState({
        usertypeValidation: '',
        usernameValidation: '',
        emailValidation: '',
        passwordValidation: '',
        confirmPasswordValidation: '',
        //phoneValidation: '',
        //firstnameValidation: '',
        //lastnameValidation: '',
        //middlenameValidation: '',
      });
    }

    let errors = [
      /*validate usertype
      {
        field: 'usertype',
        signUpModeOnly: true,
        validate:
          function() {
            self.setState({
              usertypeValidation: '',
            });
            if(Validator.isEmpty(self.state.usertype, {ignore_whitespace:true})){
              self.setState({
                usertypeValidation: "User type is required",
              });
              isValid = false;
            }
          }
      },
      */

      /*validate username*/
      {
        field: 'username',
        signUpModeOnly: false,
        validate:
          function () {
            let min = 7;
            let max = 50;

            self.setState({
              usernameValidation: '',
            });
            if (Validator.isEmpty(self.state.username, { ignore_whitespace: true })) {
              self.setState({
                usernameValidation: "Username is required",
              });
              isValid = false;
            }
            else if (self.state.signUpMode &&
              !Validator.isLength(self.state.username, { min: min, max: max })) {
              self.setState({
                usernameValidation: "Username is has to be between " + min + " and " + max + " characters in length",
              });
              isValid = false;
            }
          }
      },

      /*validate email*/
      {
        field: 'email',
        signUpModeOnly: true,
        validate:
          function () {
            let min = 6;
            let max = 50;
            self.setState({
              emailValidation: '',
            });
            if (Validator.isEmpty(self.state.email, { ignore_whitespace: true })) {
              self.setState({
                emailValidation: "Email is required",
              });
              isValid = false;
            }
            else if (!Validator.isEmail(self.state.email)) {
              self.setState({
                emailValidation: "Valid email is required",
              });
              isValid = false;
            }
          }
      },

      /*validate firstname
      {
        field: 'firstname',
        signUpModeOnly: true,
        validate:
          function() {
            let min = 4;
            let max = 50;

            self.setState({
              firstnameValidation: '',
            });
            if(Validator.isEmpty(self.state.firstname, {ignore_whitespace:true})){
              self.setState({
                firstnameValidation: "Firstname is required",
              });
              isValid = false;
            }
            else if(!Validator.isLength(self.state.firstname, {min:min, max: max})){
              self.setState({
                firstnameValidation: "Firstname has to be between " + min + " and " + max + " characters in length",
              });
              isValid = false;
            }
          }
        },

        /*validate lastname
        {
          field: 'lastname',
          signUpModeOnly: true,
          validate:
            function() {
              let min = 4;
              let max = 50;

              self.setState({
                lastnameValidation: '',
              });
              if(Validator.isEmpty(self.state.lastname, {ignore_whitespace:true})){
                self.setState({
                  lastnameValidation: "Lastname is required",
                });
                isValid = false;
              }
              else if(!Validator.isLength(self.state.firstname, {min:min, max: max})){
                self.setState({
                  lastnameValidation: "Lastname has to be between " + min + " and " + max + " characters in length",
                });
                isValid = false;
              }
            }
          },
          */

      /*validate password*/
      {
        field: 'password',
        signUpModeOnly: false,
        validate:
          function () {
            let min = 8;
            let max = 15;

            self.setState({
              passwordValidation: '',
              confirmPasswordValidation: '',
            });
            if (Validator.isEmpty(self.state.password, { ignore_whitespace: true })) {
              self.setState({
                passwordValidation: "Password is required",
              });
              isValid = false;
            }
            else if (self.state.signUpMode) {
              if (!Validator.isLength(self.state.password, { min: min, max: max })) {
                self.setState({
                  passwordValidation: "Password is has to be between " + 8 + " and " + max + " characters in length",
                });
                isValid = false;
              }
              else if (Validator.matches(self.state.password, /^[^A-Z]+$/)) {
                self.setState({
                  passwordValidation: "Password must contain at least one capital letter",
                });
                isValid = false;
              }
              else if (Validator.matches(self.state.password, /^[^a-z]+$/)) {
                self.setState({
                  passwordValidation: "Password must contain at least a small letter",
                });
                isValid = false;
              }
              else if (Validator.matches(self.state.password, /^[^0-9]+$/)) {
                self.setState({
                  passwordValidation: "Password must contain at least a number",
                });
                isValid = false;
              }
              else if (Validator.matches(self.state.password, /^[^!@#$&*]+$/)) {
                self.setState({
                  passwordValidation: "Password must contain at least one special symbol [!@#$&*]",
                });
                isValid = false;
              }
              /*check confirm password*/
              else if (!Validator.isEmpty(self.state.confirmPassword, { ignore_whitespace: true }) &&
                !Validator.equals(self.state.password, self.state.confirmPassword)) {
                self.setState({
                  confirmPasswordValidation: "Password does not match",
                });
                isValid = false;
              }
            }
          }
      },

      /*validate confirm password*/
      {
        field: 'confirmPassword',
        signUpModeOnly: true,
        validate:
          function () {

            self.setState({
              confirmPasswordValidation: '',
            });
            var isPasswordValid = !Validator.isEmpty(self.state.password, { ignore_whitespace: true }) &&
              self.state.passwordValidation === '';
            if (isPasswordValid) {
              if (Validator.isEmpty(self.state.confirmPassword, { ignore_whitespace: true })) {
                self.setState({
                  confirmPasswordValidation: "Please confirm password",
                });
                isValid = false;
              }
              else if (!Validator.equals(self.state.password, self.state.confirmPassword)) {
                self.setState({
                  confirmPasswordValidation: "Password does not match",
                });
                isValid = false;
              }
            }
          }
      },

      /*validate phone
      {
        field: 'phone',
        signUpModeOnly: true,
        validate:
          function() {
            self.setState({
              phoneValidation: '',
            });

            if(Validator.isEmpty(self.state.phone, {ignore_whitespace:true})){
              self.setState({
                phoneValidation: "Phone number is required",
              });
              isValid = false;
            }
            else if(!Validator.isMobilePhone(self.state.phone, ['en-US'])){
              self.setState({
                phoneValidation: "Phone number is invalid",
              });
              isValid = false;
            }
          }
        },
    */
    ];

    let toValidate = errors.filter(function (item) {
      return (
        (!item.signUpModeOnly ||
          self.state.signUpMode)
        &&
        (!field || item.field == field)
      );
    });

    for (var i = 0; i < toValidate.length; i++) {
      var funcToRun = toValidate[i];
      funcToRun.validate();
    }

    return isValid;
  }

  signupHandler = (evt) => {
    this.setState(previousState => ({
      signUpMode: !this.state.signUpMode,

      //usertypeValidation: '',
      usernameValidation: '',
      emailValidation: '',
      passwordValidation: '',
      confirmPasswordValidation: '',
      //phoneValidation: '',
      //firstnameValidation: '',
      //lastnameValidation: '',
      //middlenameValidation: '',
    }
    ));
  }

  signInHandler = (evt) => {
    var isValid = this.validate();
    if (!isValid) {
      return;
    }

    if (this.state.signUpMode) {
      this.signUpNewUser();
    }
    else {
      this.signInExistingUser();
    }
  }

  signUpNewUser() {
    var usertype = "";
    if (this.props.usertype == "Rider") {
      usertype = "CLIENT";
    }
    else if (this.props.usertype == "Triver") {
      usertype = "TRIVER";
    }

    let url = this.state.signUpNewURL;
    let body = {
      "id": "",
      "username": this.state.username,
      "password": this.state.password,
      "firstname": 'default',//this.state.firstname,
      "lastname": 'default',//this.state.lastname,
      "middlename": 'default',//"default",
      "email": this.state.email,
      "phone": this.state.phone,
      "usertype": usertype,
      "enabled": 1,
      "lastPasswordResetDate": null,
      "authorities": null,
      "online": 0
    }
    let options = {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "cache-control": "no-cache"
      },
      body: JSON.stringify(body)
    }

    this.ajaxCall(url, options, this.signupCallback);
  }

  signInExistingUser() {
    var usertype = "";
    if (this.props.usertype == "Rider") {
      usertype = "CLIENT";
    }
    else if (this.props.usertype == "Triver") {
      usertype = "TRIVER";
    }

    let url = this.state.signInExistingURL;
    let body = {
      "username": this.state.username,
      "password": this.state.password,
      "usertype": usertype
    }
    let options = {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "cache-control": "no-cache"
      },
      body: JSON.stringify(body)
    }

    this.ajaxCall(url, options, this.signinCallback);
  }

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  handleClickShowConfirmPassword = () => {
    this.setState(state => ({ showConfirmPassword: !state.showConfirmPassword }));
  };

  handleChangeSignInInfo = (prop, validator) => event => {
    this.setState({ [prop]: event.target.value });
    if (validator) {
      this.setState({ [validator]: '' });
    }
  };

  handleBlur = (prop) => event => {
    this.validate(prop);
  }

  signinCallback = (result) => {

    let userinfo = result;
    let usertype = "";
    if (this.props.usertype == "Rider") {
      usertype = "CLIENT";
    }
    else if (this.props.usertype == "Triver") {
      usertype = "TRIVER";
    }

    if (userinfo.id) {
      let returnedUserType = userinfo.userType.toUpperCase();
      let usertypeResponse = returnedUserType == 'TRIVER' ? "Driver" : "Rider";
      if (usertype == returnedUserType) {
        this.props.updateUserInfo(userinfo.id,
          userinfo.token,
          usertypeResponse,
          userinfo.email,
          userinfo.username);
        this.clear();
        this.props.redirect();
      }
      else {
        this.setState(previousState => ({
          errorSigning: true
        }
        ))
      }
    }
  }

  signupCallback = () => {
    this.setState(previousState => ({
      signUpMode: false,
    }
    ));
    this.signInExistingUser();
  }

  clear = () => {
    this.setState(prevState => ({
      username: '',
      email: '',
      password: '',
      confirmPassword: '',
      showPassword: false,
      showconfirmPassword: false,
      //phone: '',
      //firstname: '',
      //lastname: '',
      //middlename: '',

      signUpMode: false,
      usertype: '',

      //usertypeValidation: '',
      usernameValidation: '',
      emailValidation: '',
      passwordValidation: '',
      confirmPasswordValidation: '',
      //phoneValidation: '',
      //firstnameValidation: '',
      //lastnameValidation: '',
      //middlenameValidation: '',
    }))
  }

  handleErrorMessageClose() {
    this.setState(previousState => ({
      errorSigning: false
    }
    ))
  }

  toggleLoading(loading) {
    this.setState(prevState => (
      {
        isLoading: loading,
      }));
  }

  ajaxCall = (url, options, callback) => {
    let self = this;
    self.setState(previousState => ({
      data: "",
      errorSigning: false,
      isLoading: true
    }
    ))

    fetch(url, options)
      .then(function (response) {

        return response.json();
      })
      .then(function (result) {
        if (!result.error) {
          self.setState({
            isLoading: false,
          });

          callback(result);
        }
        else {
          self.setState({
            errorSigning: true,
            isLoading: false,
          });
        }
      })
      .catch(function (error) {
        console.log(error);
        self.setState({
          errorSigning: true,
          isLoading: false
        });
      });
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Card className={classes.card}>
          <CardContent>
            <form className={classes.container} noValidate autoComplete="off">
              {/*
                      {this.state.signUpMode
                        ?
                          <FormControl className={classes.formControl} variant="outlined">
                            <InputLabel ref={ref => {
                                  this.InputLabelRef = ref;
                                }}
                                helperText={this.state.usertypeValidation}
                                error={this.state.usertypeValidation !== ""}
                                htmlFor="usertype">
                                Sign up as (User type)
                            </InputLabel>
                            <Select
                              helperText={this.state.usertypeValidation}
                              error={this.state.usertypeValidation !== ""}
                              value={this.state.usertype}
                              onChange={this.handleChangeSignInInfo('usertype', 'usertypeValidation')}
                              onBlur={this.handleBlur('usertype')}
                              input={
                                  <OutlinedInput
                                    name="usertype"
                                    id="usertype"
                                    labelWidth={this.state.labelWidth}
                                  />
                                }>
                              <MenuItem value="">Select</MenuItem>
                              <MenuItem value="Rider">Rider</MenuItem>
                              <MenuItem value="Driver">Driver</MenuItem>
                            </Select>
                          </FormControl>
                        : null
                      }
                      */}

              <TextField
                helperText={this.state.usernameValidation}
                error={this.state.usernameValidation !== ""}
                label="User Name"
                margin="normal"
                variant="outlined"
                fullWidth
                value={this.state.username}
                onChange={this.handleChangeSignInInfo('username', 'usernameValidation')}
                onBlur={this.handleBlur('username')}
              /*
              InputProps={{
                  endAdornment: (
                      <InputAdornment position="end">
                          <ThumbUpIcon
                              tabIndex={-1}
                              aria-label="Toggle password visibility"
                              //onClick={this.handleClickShowPassword}
                              >
                              <Visibility />
                          </ThumbUpIcon>
                      </InputAdornment>
                  ),
              }}
              */
              />


              {this.state.signUpMode
                ?
                <TextField
                  helperText={this.state.emailValidation}
                  error={this.state.emailValidation !== ""}
                  label="Email"
                  margin="normal"
                  variant="outlined"
                  fullWidth
                  value={this.state.email}
                  onChange={this.handleChangeSignInInfo('email', 'emailValidation')}
                  onBlur={this.handleBlur('email')}
                />
                : null
              }

              {/*
                        {this.state.signUpMode
                          ?
                            <TextField
                                helperText={this.state.firstnameValidation}
                                error={this.state.firstnameValidation !== ""}
                                label="Firstname"
                                margin="normal"
                                variant="outlined"
                                fullWidth
                                value={this.state.firstname}
                                onChange={this.handleChangeSignInInfo('firstname', 'firstnameValidation')}
                                onBlur={this.handleBlur('firstname')}
                            />
                          : null
                        }

                        {this.state.signUpMode
                          ?
                            <TextField
                                helperText={this.state.lastnameValidation}
                                error={this.state.lastnameValidation !== ""}
                                label="Lastname"
                                margin="normal"
                                variant="outlined"
                                fullWidth
                                value={this.state.lastname}
                                onChange={this.handleChangeSignInInfo('lastname', 'lastnameValidation')}
                                onBlur={this.handleBlur('lastname')}
                            />
                          : null
                        }
                        */}

              <TextField
                label="Password"
                margin="normal"
                variant="outlined"
                helperText={this.state.passwordValidation}
                error={this.state.passwordValidation !== ""}
                type={this.state.showPassword ? 'text' : 'password'}
                value={this.state.password}
                onChange={this.handleChangeSignInInfo('password', 'passwordValidation')}
                onBlur={this.handleBlur('password')}
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        tabIndex={-1}
                        aria-label="Toggle password visibility"
                        onClick={this.handleClickShowPassword}>
                        {this.state.showPassword
                          ? <VisibilityOff />
                          : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />

              {this.state.signUpMode
                ?
                <TextField
                  label="Confirm Password"
                  margin="normal"
                  variant="outlined"
                  helperText={this.state.confirmPasswordValidation}
                  error={this.state.confirmPasswordValidation !== ""}
                  type={this.state.showConfirmPassword ? 'text' : 'password'}
                  value={this.state.confirmPassword}
                  onChange={this.handleChangeSignInInfo('confirmPassword', 'confirmPasswordValidation')}
                  onBlur={this.handleBlur('confirmPassword')}
                  fullWidth
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          tabIndex={-1}
                          aria-label="Toggle password visibility"
                          onClick={this.handleClickShowConfirmPassword}>
                          {this.state.showConfirmPassword
                            ? <VisibilityOff />
                            : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
                : null
              }


              {/*

                            {this.state.signUpMode
                              ?
                                <TextField
                                    label="Phone"
                                    margin="normal"
                                    variant="outlined"
                                    value={this.state.phone}
                                    helperText={this.state.phoneValidation}
                                    error={this.state.phoneValidation !== ""}
                                    onChange={this.handleChangeSignInInfo('phone', 'phoneValidation')}
                                    onBlur={this.handleBlur('phone')}
                                    fullWidth
                                />
                                : null
                              }
                            */}

              <Button onClick={() => this.signInHandler()}
                size="medium"
                variant="contained"
                color="primary"
                className={classes.button}>
                {this.state.signUpMode ? "Sign up" : "Sign in"}
              </Button>
            </form>

            <span>
              {
                this.state.signUpMode
                  ? "Already have an account?"
                  : "Don't have an account?"
              } &nbsp;
                      <a onClick={this.signupHandler}
                className={classes.action}>
                {this.state.signUpMode ? "Sign in" : "Sign up"}
              </a></span>
          </CardContent>
        </Card>

        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
          open={this.state.errorSigning}
          autoHideDuration={5000}
          onClose={this.handleErrorMessageClose}
          message={
            <span>
              {this.state.signUpMode
                ? "There was an error signing up"
                : "Plese provide valid credentials"}
            </span>
          }
        />

        <Dialog
          open={this.state.isLoading}
          aria-labelledby="responsive-dialog-title">
          <DialogContent>
            <CircularProgress />
          </DialogContent>
        </Dialog>
      </div>
    );
  }

  /*
  // -------------------------------------------------------
  driverSignupHandler = (evt) => {

      let token = "bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzYW5AY2NjLmNvbSIsImV4cCI6MTU0MjUxMDA3OSwiaWF0IjoxNTQxOTA1Mjc5fQ.TvELbkd_EL9LOpjiSgJyYsSQdjz3wSfJW49E9JY5E-p4OC1PhoG4b0ZZISI3p6Q3m_RyfTBetKy73_4Kkk-oZg";
      let opts = {
          "username": "san@ccc.com",
          "password": "ssssss"
      }

      evt.preventDefault();
      this.props.history.push('Driver?nav=1');
  }

  riderSignupHandler = (evt) => {
      evt.preventDefault();
      this.props.history.push('Rider');
  }

  driverSignInHandler = (evt) => {

      let opts = {
          "username": this.state.userNameDriver,
          "password": this.state.passwordDriver
      }
      let self = this;

      axios.post('/api/auth', opts)
          .then(function (data) {
              console.log(data);
              self.props.history.push('Driver?nav=2');
          })
  }


  riderSignInHandler = (evt) => {
      this.props.history.push('Rider');
  }

  handleClickShowPasswordRider = () => {
      this.setState(state => ({ showPasswordRider: !state.showPasswordRider }));
  };
  handleClickShowPasswordDriver = () => {
      this.setState(state => ({ showPasswordDriver: !state.showPasswordDriver }));
  };

  handleChangeDriver = prop => event => {
      this.setState({ [prop]: event.target.value });
  };

  handleChangeRider = prop => event => {
      this.setState({ [prop]: event.target.value });
  };



  render() {
      const { classes } = this.props;
      return (
          <section className={classes.background + " mbr-section mbr-section__container article header3-c"} id="SignupLinkMain">
              <div className="container">
                  <div className="row">
                      <div className="col-xs-12">
                          <h3 className="mbr-section-title">Sign in</h3>
                          <div className={classes.root}>
                              <Grid container spacing={24}>
                                  <Grid item xs={12} sm={6}>
                                      <h4>
                                          <a>Triver</a>
                                      </h4>
                                      <Card className={classes.card}>
                                          <CardContent>
                                              <form className={classes.container} noValidate autoComplete="off">
                                                  <TextField
                                                      id="outlined-name"
                                                      label="User Name"
                                                      margin="normal"
                                                      variant="outlined"
                                                      fullWidth
                                                      value={this.state.userNameDriver}
                                                      onChange={this.handleChangeDriver('userNameDriver')}
                                                  />
                                                  <TextField
                                                      id="outlined-name"
                                                      label="Password"
                                                      margin="normal"
                                                      variant="outlined"
                                                      type={this.state.showPasswordDriver ? 'text' : 'password'}
                                                      value={this.state.passwordDriver}
                                                      onChange={this.handleChangeDriver('passwordDriver')}
                                                      fullWidth
                                                      InputProps={{
                                                          endAdornment: (
                                                              <InputAdornment position="end">
                                                                  <IconButton
                                                                      aria-label="Toggle password visibility"
                                                                      onClick={this.handleClickShowPasswordDriver}
                                                                  >
                                                                      {this.state.showPasswordDriver ? <VisibilityOff /> : <Visibility />}
                                                                  </IconButton>
                                                              </InputAdornment>
                                                          ),
                                                      }}
                                                  />
                                                  <Button
                                                      size="medium"
                                                      variant="contained"
                                                      color="primary"
                                                      className={classes.button}
                                                      onClick={this.driverSignInHandler}
                                                  >
                                                      Sign in
                                          </Button>
                                              </form>

                                              <span>Don't have an account? <a href="#" onClick={this.driverSignupHandler}> Signup</a></span>
                                          </CardContent>
                                      </Card>
                                  </Grid>
                                  <Grid item xs={12} sm={6}>
                                      <h4>
                                          <a>Rider</a>
                                      </h4>
                                      <Card className={classes.card}>
                                          <CardContent>
                                              <form className={classes.container} noValidate autoComplete="off">
                                                  <TextField
                                                      id="outlined-name"
                                                      label="User Name"
                                                      margin="normal"
                                                      variant="outlined"
                                                      fullWidth
                                                      value={this.state.userNameRider}
                                                      onChange={this.handleChangeDriver('userNameRider')}
                                                  />
                                                  <TextField
                                                      id="outlined-name"
                                                      label="Password"
                                                      margin="normal"
                                                      variant="outlined"
                                                      type={this.state.showPasswordRider ? 'text' : 'password'}
                                                      value={this.state.passwordRider}
                                                      onChange={this.handleChangeRider('passwordRider')}
                                                      fullWidth
                                                      InputProps={{
                                                          endAdornment: (
                                                              <InputAdornment position="end">
                                                                  <IconButton
                                                                      aria-label="Toggle password visibility"
                                                                      onClick={this.handleClickShowPasswordRider}
                                                                  >
                                                                      {this.state.showPasswordRider ? <VisibilityOff /> : <Visibility />}
                                                                  </IconButton>
                                                              </InputAdornment>
                                                          ),
                                                      }}
                                                  />
                                                  <Button size="medium" variant="contained" color="primary" className={classes.button}>
                                                      Sign in
                                          </Button>
                                              </form>
                                              <span>Don't have an account? <a href="#" onClick={this.riderSignupHandler}> Signup</a></span>
                                          </CardContent>
                                      </Card>
                                  </Grid>
                              </Grid>
                          </div>
                          <small className="mbr-section-subtitle"></small>
                      </div>
                  </div>
              </div>
          </section>
      );
  }
  */
}

SigninForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

// export default Signin;
export default withStyles(styles)(SigninForm);
