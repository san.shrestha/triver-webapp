import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
      },
      table: {
        minWidth: 500,
      },
    paper: {
        padding: 30,
        textAlign: 'center',
        color: 'red',
    },
    pageTitle: {
        paddingTop: 10,
        paddingBottom: 20
    }
});


  
  let id = 0;
  let refId = 0;
  function createData(date, tourId, amount) {
    id += 1;
    return { date, tourId, amount };
  }

  function createDataRewards(reffId, name, amount, date) {
    refId += 1;
    return { reffId, name, amount, date};
  }
  
  const rows = [
    createData( '09/14/2018','#343434', 100 ),
    createData( '09/15/2018','#566563', 200 ),
    createData( '09/18/2018','#887393', 300 ),
    createData( '09/20/2018', '#233233', 300 ),
    createData( '09/30/2018', '#343434', 250 ),
  ];

  const referral = [
    createDataRewards( '#343434', 'John Smith', '09/04/2018', 200 ),
    createDataRewards( '#566563', 'Peter Ray', '09/15/2018', 200 ),
    createDataRewards( '#887393', 'Harry Pete', '09/18/2018', 200 ),
    createDataRewards( '#233233', 'Bret Cannon', '09/20/2018',  200 ),
  ];

const earnings = (props) => {
    const { classes } = props;
    return (
        <div className={classes.background}>
            <div className="container">
                <h2 className={classes.pageTitle}>My Earnings</h2>
                <Paper className={classes.root}>
                    <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>
                            </Grid>
                    </Grid>
                    <Grid container spacing={24}>
                            <Grid item xs={8} sm={4}>
                                <form className={classes.container} noValidate>
                                    <TextField
                                        id="date"
                                        label="From"
                                        type="date"
                                        defaultValue="2018-05-24"
                                        className={classes.textField}
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                </form>
                            </Grid>
                            <Grid item xs={8} sm={4}>
                                <form className={classes.container} noValidate>
                                    <TextField
                                        id="date"
                                        label="From"
                                        type="date"
                                        defaultValue="2018-10-24"
                                        className={classes.textField}
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                </form>
                            </Grid>
                    </Grid>
                    <Grid container spacing={24} style={
                            {
                                paddingLeft:40, 
                                marginTop:20,
                                marginBottom:20, 
                                backgroundColor:'#cccccc',
                                paddingTop:15,
                                paddingBottom:10
                            }}>
                            <h4>Your total earning for selected date range is </h4>
                            <h4 style={{paddingLeft:20}}>1150</h4>
                    </Grid>

                
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Date</TableCell>
                                <TableCell numeric>Tour ID</TableCell>
                                <TableCell numeric>Amount</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map(row => {
                                return (
                                    <TableRow key={row.id}>
                                        <TableCell >{row.date}</TableCell>
                                        <TableCell numeric>{row.tourId}</TableCell>
                                        <TableCell numeric>{row.amount}</TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>

                    <Grid container spacing={24} style={
                            {
                                paddingLeft:40, 
                                marginTop:20,
                                marginBottom:20, 
                                backgroundColor:'#cccccc',
                                paddingTop:15,
                                paddingBottom:10
                            }}>
                            <h4>Referral amount: 800</h4>
                    </Grid>

                           
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Referral Id</TableCell>
                                <TableCell>Triver Name</TableCell>
                                <TableCell numeric>Referral Amount</TableCell>
                                <TableCell>Referral Date</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {referral.map(referral => {
                                return (
                                    <TableRow key={referral.refId}>
                                        <TableCell>{referral.reffId}</TableCell>
                                        <TableCell>{referral.name}</TableCell>
                                        <TableCell numeric>{referral.amount}</TableCell>
                                        <TableCell>{referral.date}</TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </Paper>

            </div>
        </div>
    )


};

// export default earnings
export default withStyles(styles)(earnings);

